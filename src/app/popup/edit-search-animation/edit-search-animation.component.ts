/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {SignLayoutConfig} from '../../_models/sign-layout-config';
import {SignLayout} from '../../_models/sign-layout';
import {SignEditComponent} from '../sign-edit/sign-edit.component';

@Component({
  selector: 'app-edit-search-animation',
  templateUrl: './edit-search-animation.component.html',
  styleUrls: ['./edit-search-animation.component.css']
})
export class EditSearchAnimationComponent implements OnInit {
  config: SignLayoutConfig;
  value: number = 0;

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,
              public dialogRef: MatDialogRef<EditSearchAnimationComponent>, @Inject(MAT_DIALOG_DATA) private data: SignLayoutConfig) {
    this.config = data;
  }

  ngOnInit() {
  }
  openEdit(layout: SignLayout) {
    this.dialog.open(SignEditComponent, {
      data: layout,
    });
  }
  replaceColor(value: string) {

    return value
      .replace(/&4/g, '<span style="color: #AA0000; text-shadow: 1px 1px 0 #2A0000;" >')
      .replace(/&c/g, '<span style="color: #FF5555; text-shadow: 1px 1px 0 #3F1515;" >')
      .replace(/&6/g, '<span style="color: #FFAA00; text-shadow: 1px 1px 0 #2A2A00;" >')
      .replace(/&e/g, '<span style="color: #FFFF55; text-shadow: 1px 1px 0 #3F3F15;" >')
      .replace(/&2/g, '<span style="color: #00AA00; text-shadow: 1px 1px 0 #002A00;" >')
      .replace(/&a/g, '<span style="color: #55FF55; text-shadow: 1px 1px 0 #153F15;" >')
      .replace(/&b/g, '<span style="color: #55FFFF; text-shadow: 1px 1px 0 #153F3F;" >')
      .replace(/&3/g, '<span style="color: #00AAAA; text-shadow: 1px 1px 0 #002A2A;" >')
      .replace(/&1/g, '<span style="color: #0000AA; text-shadow: 1px 1px 0 #00002A;" >')
      .replace(/&9/g, '<span style="color: #5555FF; text-shadow: 1px 1px 0 #15153F;" >')
      .replace(/&d/g, '<span style="color: #FF55FF; text-shadow: 1px 1px 0 #3F153F;" >')
      .replace(/&5/g, '<span style="color: #AA00AA; text-shadow: 1px 1px 0 #2A002A;" >')
      .replace(/&7/g, '<span style="color: #AAAAAA; text-shadow: 1px 1px 0 #2A2A2A;" >')
      .replace(/&8/g, '<span style="color: #555555; text-shadow: 1px 1px 0 #151515;" >')
      .replace(/&0/g, '<span style="color: #000000; text-shadow: 1px 1px 0 #000000;" >')
      .replace(/&f/g, '<span style="color: #FFFFFF; text-shadow: 1px 1px 0 #3F3F3F;" >')

      .replace(/&l/g, '<span style="font-weight: bold">')
      .replace(/&n/g, '<span style="text-decoration: underline">')
      .replace(/&o/g, '<span style="font-style: italic;">')
      .replace(/&m/g, '<span style="text-decoration: line-through">')
      .replace(/&r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/§4/g, '<span style="color: #fe0000">')
      .replace(/§c/g, '<span style="color: #ff4c52">')
      .replace(/§6/g, '<span style="color: #ff8e42">')
      .replace(/§e/g, '<span style="color: #ffd800">')
      .replace(/§2/g, '<span style="color: #31a310">')
      .replace(/§a/g, '<span style="color: #4dd800">')
      .replace(/§b/g, '<span style="color: #01ffff">')
      .replace(/§3/g, '<span style="color: #0094fe">')
      .replace(/§1/g, '<span style="color: #0026ff">')
      .replace(/§9/g, '<span style="color: #4368ff">')
      .replace(/§d/g, '<span style="color: #fe00dc">')
      .replace(/§5/g, '<span style="color: #b100fe">')
      .replace(/§7/g, '<span style="color: #a0a0a0">')
      .replace(/§8/g, '<span style="color: #404040">')
      .replace(/§0/g, '<span style="color: #010101">')
      .replace(/§f/g, '<span style="color: #ffffff">')

      .replace(/§l/g, '<span style="font-weight: bold">')
      .replace(/§n/g, '<span style="text-decoration: underline">')
      .replace(/§o/g, '<span style="font-style: italic;">')
      .replace(/§m/g, '<span style="text-decoration: line-through">')
      .replace(/§r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      ;
  }

  addFrame() {
    this.config.searchingAnimation.searchingLayouts.push({
      signLayout: [
        "LINE - 1 ",
        "LINE - 2 ",
        "LINE - 3 ",
        "LINE - 4 "
      ],
      blockId: 159,
      subId: 0,
      name: 'loading'+(this.config.searchingAnimation.searchingLayouts.length+1)
    });
  }

  removeFrame(value: number) {
    this.config.searchingAnimation.searchingLayouts.splice(value,1);
    this.value = this.config.searchingAnimation.searchingLayouts.length -1;
  }
}

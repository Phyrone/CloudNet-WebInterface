/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MatDialog} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {FormControl, FormGroup} from '@angular/forms';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';


@Component({
  selector: 'app-player-console',
  templateUrl: './player-console.component.html',
  styleUrls: ['./player-console.component.css']
})
export class PlayerConsoleComponent implements OnInit {

  sender = new FormGroup({
    player: new FormControl(),
    server: new FormControl(),
  });

  constructor(public _cloud: CloudConnectorService, public dialog: MatDialog,
              public _notify: NotificationsService,
              private cookie: CookieService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('popup','player_console');
  }
  ngOnInit() {
  }
  onSend() {
    this._cloud.postSendPlayer(JSON.parse(this.cookie.get("currentUser")),this.getServer(),this.getPlayer()).subscribe(()=>{

    },error =>{
      if (error.status === 403) {
        this._notify.AccessDenied();
      }
    })
  }
  getPlayer(){
    return this.sender.get('player').value;
  }
  getServer(){
    return this.sender.get('server').value;
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {Wrapper} from '../../_models/wrapper';

@Component({
  selector: 'app-wrapper-detail-view',
  templateUrl: './wrapper-detail-view.component.html',
  styleUrls: ['./wrapper-detail-view.component.css']
})
export class WrapperDetailViewComponent implements OnInit {

  public wrapper: Wrapper;
  serverDisplayedColumns: String[] = ['serverid', 'onlinemode', 'prioritystop', 'onlinecount', 'serverstate'];
  cloudDisplayedColumns: String[] = ['serverid','onlinecount','serverstate'];
  proxyDisplayedColumns: string[] = ['serverid', 'onlinecount'];

  constructor(public dialogRef: MatDialogRef<WrapperDetailViewComponent>, public _cloud: CloudConnectorService,
              @Inject(MAT_DIALOG_DATA) public element: Wrapper) {
    this.wrapper = element;
  }

  ngOnInit() {

  }

}

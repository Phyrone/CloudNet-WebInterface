/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit} from '@angular/core';
import {MatDialogRef} from '@angular/material';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../_services/app-config.service';

@Component({
  selector: 'app-masterstop',
  templateUrl: './master-stop-dialog.component.html',
  styleUrls: ['./master-stop-dialog.component.css']
})
export class MasterStopDialogComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MasterStopDialogComponent>, public _cloud: CloudConnectorService,
              private _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {

  }


  ngOnInit() {
  }

  onCloseClick() {
    this.dialogRef.close(false);
  }

  onStop() {
    this._cloud.postMasterStop(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close();
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, ElementRef, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {NotificationsService} from '../../_services/notifications.service';
import {Observable, Subscription} from 'rxjs';
import {FormControl, FormGroup} from '@angular/forms';
import 'rxjs-compat/add/observable/interval';
import {MasterStopDialogComponent} from '../masterstop/master-stop-dialog.component';
import 'rxjs-compat/add/observable/fromEvent';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-master-interface',
  templateUrl: './master-interface.component.html',
  styleUrls: ['./master-interface.component.css']
})
export class MasterInterfaceComponent implements OnInit, OnDestroy {
  @ViewChild('console') console: ElementRef;
  lines: string[] = [];
  value = '';
  public locked = true;
  private timer: Subscription;
  interface = new FormGroup({
    command: new FormControl()
  });
  lockedColor = 'primary';
  private keyEvent: Subscription;
  constructor(public dialogRef: MatDialogRef<MasterInterfaceComponent>, public _cloud: CloudConnectorService, private renderer: Renderer2,
              private _notify: NotificationsService, public dialog: MatDialog,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('popup','mater_interface');
  }
  ngOnInit() {
    this.timer = Observable.interval(parseInt(localStorage.getItem('console.interval'))).subscribe(() => {
      this._cloud.getMasterLog(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
        this.lines = t['response'];
        if (this.locked) {
          this.console.nativeElement.scrollTop = this.console.nativeElement.scrollHeight;
        }
      }, error => {
        if (error.status === 403) {
          this.dialogRef.close();
        }
      });
    });
    this.keyEvent = Observable.fromEvent(document, 'keypress').subscribe(e => {
      if (e['keyCode'] === 13) {
        this.onSendCommand();
      }
      if (e['keyCode'] === 53) {
        this.onReloadAll();
      }
    });
  }
  ngOnDestroy() {
    this.keyEvent.unsubscribe();
  }
  SwapScroll() {
    this.locked = !this.locked;
    if (this.locked === false) {
      // @ts-ignore
      this.lockedColor = '';
    }
    if (this.locked === true) {
      this.lockedColor = 'primary';
    }
  }

  onReloadAll() {
    this._cloud.postMasterReloadAll(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(() => {
    }, error1 => {
      if (error1.status === 403) {
        this._notify.AccessDenied();
      }
    });
  }

  onStopMaster() {
    this.dialog.open(MasterStopDialogComponent, {
      hasBackdrop: false
    });
  }

  onClearWrapperCache() {
    this._cloud.postMasterClearCache(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(() => {
    }, error1 => {
      if (error1.status === 403) {
        this._notify.AccessDenied();
      }
    });
  }

  onSendCommand() {
    const command = this.Command.value;
    this.Command.reset();
    this._cloud.postMasterCommand(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), command).subscribe(() => {
    }, error1 => {
      if (error1.status === 403) {
        this._notify.AccessDenied();
      }
    });
  }
  get Command() {
    return this.interface.get('command');
  }
}

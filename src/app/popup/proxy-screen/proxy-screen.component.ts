/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, ElementRef, Inject, OnDestroy, OnInit, Renderer2, ViewChild} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {NotificationsService} from '../../_services/notifications.service';
import {Observable, Subscription} from 'rxjs';
import {SimpleProxyGroup} from '../../_models/simple-proxy-group';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';

@Component({
  selector: 'app-proxy-screen',
  templateUrl: './proxy-screen.component.html',
  styleUrls: ['./proxy-screen.component.css']
})
export class ProxyScreenComponent implements OnInit, OnDestroy {
  @ViewChild('console') console: ElementRef;
  lockedColor = 'primary';
  lock = true;
  lines: string[] = [];
  commandValue: string;
  private timer: Subscription;

  constructor(public dialogRef: MatDialogRef<ProxyScreenComponent>, public _cloud: CloudConnectorService, private renderer: Renderer2,
              private _notify: NotificationsService, @Inject(MAT_DIALOG_DATA) public _proxy: SimpleProxyGroup,
              private cookie: CookieService,private _es: ElectronService) {
  }

  ngOnInit() {

    this.timer = Observable.interval(parseInt(localStorage.getItem('dashboard.console'))).subscribe(() => {
      this._cloud.getProxyConsole(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this._proxy.serviceId.serverId).subscribe(t => {
        this.lines = t['response'] as string[];
        if (this.lock) {
          this.console.nativeElement.scrollTop = this.console.nativeElement.scrollHeight;
        }
      }, error => {
        if (error.status === 403) {
          this._notify.AccessDenied();
          this.dialogRef.close();
        }
      });
    });
  }

  ngOnDestroy() {
    this.timer.unsubscribe();
    this._cloud.postDisableScreenProxy(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this._proxy.serviceId.serverId);
  }

  SwapScroll() {
    this.lock = !this.lock;
    if (this.lock === false) {
      // @ts-ignore
      this.lockedColor = '';
    }
    if (this.lock === true) {
      this.lockedColor = 'primary';
    }
  }

  onSubmit(command: string) {
    this.commandValue = '';
    this._cloud.postProxyCommand(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), command, this._proxy.serviceId.serverId).subscribe(() => {

    }, error => {
      if (error.status === 403) {
        this._notify.AccessDenied();
      }
    });
  }


}

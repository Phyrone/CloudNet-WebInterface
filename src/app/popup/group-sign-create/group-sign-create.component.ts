/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {SignLayoutConfig} from '../../_models/sign-layout-config';

@Component({
  selector: 'app-group-sign-create',
  templateUrl: './group-sign-create.component.html',
  styleUrls: ['./group-sign-create.component.css']
})
export class GroupSignCreateComponent implements OnInit {

  constructor(public dialog: MatDialog, public _notify: NotificationsService,
              public dialogRef: MatDialogRef<GroupSignCreateComponent>, @Inject(MAT_DIALOG_DATA) public data: SignLayoutConfig) { }

  ngOnInit() {
  }

  AddGroup(name: string) {
    this.data.groupLayouts.push({
      name: name,
      layouts: [
        {
          signLayout: [
            "%server%",
            "&e%state%",
            "%online_players%/%max_players%",
            "%motd%"
          ],
          name: "empty",
          subId: 0,
          blockId: 159
        },
        {
          signLayout: [
            "%server%",
            "&e%state%",
            "%online_players%/%max_players%",
            "%motd%"
          ],
          name: "online",
          subId: 0,
          blockId: 159
        },
        {
          signLayout: [
            "%server%",
            "&ePREMIUM",
            "%online_players%/%max_players%",
            "%motd%"
          ],
          name: "full",
          subId: 0,
          blockId: 159
        },
        {
          signLayout: [
            "§8§m---------",
            "maintenance",
            "§cmode",
            "§8§m---------"
          ],
          name: "maintenance",
          subId: 0,
          blockId: 159
        }
      ]
    });
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {Update} from '../../_models/update';
import {MatDialogRef} from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-changelog',
  templateUrl: './changelog.component.html',
  styleUrls: ['./changelog.component.css']
})
export class ChangelogComponent implements OnInit {
  Updates: Update[];

  constructor(public dialogRef: MatDialogRef<ChangelogComponent>,
              private http: HttpClient,
              private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','changelog');
  }

  ngOnInit() {
    setTimeout(()=>{
      this.getUpdates().subscribe(updates=>{
        this.Updates = updates as Update[];
      });
    },200);
  }
  getUpdates(){
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
    });
    return this.http.get("https://assets.madfix.me/projects/MaterialDesignWebInterface/config/updates.json", {headers: headers});
  }
  onClose() {
    this.dialogRef.close();
  }

}

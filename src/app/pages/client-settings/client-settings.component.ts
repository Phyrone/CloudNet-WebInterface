/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, HostBinding, OnInit} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MatDialog, MatDialogRef} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {TranslateService} from '@ngx-translate/core';
import {FormControl, FormGroup} from '@angular/forms';
import {OverlayContainer} from '@angular/cdk/overlay';
import {Language} from '../../_models/language';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Theme} from '../../_models/theme';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-client-settings',
  templateUrl: './client-settings.component.html',
  styleUrls: ['./client-settings.component.css']
})
export class ClientSettingsComponent implements OnInit {
  themes: Theme[] = [
    {text: 'VenyMC-Theme',value: 'venymc-theme'},
    {text: 'Trash-Theme',value: 'trash-theme'},
    {text: 'Green-Theme',value: 'green-theme'},
    {text: 'Mad-Theme',value: 'mad-theme'},
    {text: 'Red-Theme',value: 'red-theme'},
    {text: 'Dark-Theme',value: 'dark-theme'},
    {text: 'Light-Theme',value: 'light-theme'}
    ];
  languages: Language[] = [];
  @HostBinding('class') componentCssClass;
  controllers = new FormGroup({
    themeControl: new FormControl(),
    languageControl: new FormControl(),
    intervalDashboard: new FormControl(),
    intervalConsole: new FormControl()
  });
  dashboardInterval: number;
  consoleInterval: number;

  constructor(public _cloud: CloudConnectorService, public dialog: MatDialog,
              public _notify: NotificationsService,
              private cookie: CookieService,
              private translate: TranslateService,
              public overlayContainer: OverlayContainer,
              private http: HttpClient,
              public dialogRef: MatDialogRef<ClientSettingsComponent>,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','client-settings');
    if (localStorage.getItem('currentTheme') != null) {
      this.controllers.get('themeControl').setValue((JSON.parse(localStorage.getItem('currentTheme')) as any).theme);
    }
    if(localStorage.getItem('dashboard.interval') == null){
      this.dashboardInterval = AppConfigService.settings.settings.interval.dashboard;
      localStorage.setItem( 'dashboard.interval', AppConfigService.settings.settings.interval.dashboard.toString());
    }else{
      this.dashboardInterval = parseInt(localStorage.getItem('dashboard.interval'));
    }
    if(localStorage.getItem('console.interval') == null){
      this.consoleInterval = AppConfigService.settings.settings.interval.console;
      localStorage.setItem( 'console.interval', AppConfigService.settings.settings.interval.console.toString());
    }else{
      this.consoleInterval = parseInt(localStorage.getItem('console.interval'));
    }
  }

  ngOnInit() {
    setTimeout(()=>{
      this.getLanguages().subscribe(langs=>{
          this.languages = langs as Language[];
          this.controllers.get('languageControl').setValue(this._es.isElectronApp?localStorage.getItem("currentLanguage"):this.cookie.get("currentLanguage"));
      });
    },200);
  }
  useLanguage(language: string) {
    this.translate.use(language);
    if(this._es.isElectronApp)
      localStorage.setItem("currentLanguage",language);
      else this.cookie.set("currentLanguage",language,0,'/');
  }
  getTheme(){
    return String(this.controllers.get('themeControl').value).toLowerCase();
  }
  getLanguage(){
    return String(this.controllers.get('languageControl').value).toLowerCase();
  }
  onSetTheme(theme) {
    this.overlayContainer.getContainerElement().classList.add(theme);
    this.componentCssClass = theme;
    localStorage.setItem('currentTheme', JSON.stringify({'theme': theme}));
    window.location.reload();
  }

  onSave() {
    this.onSetTheme(this.getTheme());
    this.useLanguage(this.getLanguage());
    window.location.reload();
    localStorage.setItem('dashboard.interval',this.dashboardInterval.toString());
    localStorage.setItem('console.interval',this.consoleInterval.toString());
  }
  getLanguages(){
    const headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
    });
    return this.http.get("https://assets.madfix.me/projects/MaterialDesignWebInterface/config/lang.json", {headers: headers});
  }

  onClose() {
    this.dialogRef.close();
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit} from '@angular/core';
import {MinecraftServer} from '../../_models/minecraft-server';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MatDialog} from '@angular/material';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {SignLayout} from '../../_models/sign-layout';
import {SignEditComponent} from '../../popup/sign-edit/sign-edit.component';
import {SignLayoutConfig} from '../../_models/sign-layout-config';
import {ServerState} from '../../_models/server-state.enum';
import {TemplateResource} from '../../_models/template-resource';
import {ServerGroupType} from '../../_models/server-group-type.enum';
import {GroupSignCreateComponent} from '../../popup/group-sign-create/group-sign-create.component';
import {Observable} from 'rxjs';
import {EditSearchAnimationComponent} from '../../popup/edit-search-animation/edit-search-animation.component';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {

  server: MinecraftServer = {
    serverInfo: {
      memory: -1,
      port: -1,
      maxPlayers: -1,
      onlineCount: -1,
      motd: 'A Minecraft Server',
      host: '127.0.0.1',
      serverState: ServerState.LOBBY,
      players: [],
      online: true,
      serverConfig: {
        extra: false,
        hideServer: false,
        startup: -1
      },
      serviceId: {
        group: 'Unknown',
        wrapperId: 'Unknown Wrapper',
        id: -1,
        serverId: 'Unknown ID',
        uniqueId: 'UUID Not fround'
      },
      template: {
        name: 'Template not Found',
        backend: TemplateResource.LOCAL,
        intallablePlugins: [],
        processPreParameters: [],
        url: ''
      }
    },
    groupMode: ServerGroupType.BUKKIT,
    processMeta: {
      onlineMode: true,
      customServerDownload: '',
      downloadablePlugins: [],
      memory: -1,
      port: -1,
      priorityStop: false,
      processParameters: [],
      serverConfig: {
        extra: false,
        hideServer: false,
        startup: -1
      },
      serviceId: {
        group: 'Unknown',
        wrapperId: 'Unknown Wrapper',
        id: -1,
        serverId: 'Unknown ID',
        uniqueId: 'UUID Not fround'
      },
      template: {
        name: 'Template not Found',
        backend: TemplateResource.LOCAL,
        intallablePlugins: [],
        processPreParameters: [],
        url: ''
      },
      url: ''
    },
    serviceId: {
      group: 'Unknown',
      wrapperId: 'Unknown Wrapper',
      id: -1,
      serverId: 'Unknown ID',
      uniqueId: 'UUID Not fround'
    }
  };
  singLayoutConfig: SignLayoutConfig = {
    fullServerHide: true,
    distance: -1,
    knockbackOnSmallDistance: false,
    strength: -1,
    groupLayouts: [
      {
        name: 'Unknown Sign Layout',
        layouts: [
          {
            name: 'Unknown',
            subId: -1,
            blockId: -1,
            signLayout: [
              'LINE 01',
              'LINE 02',
              'LINE 03',
              'LINE 04'
            ]
          }
        ]
      }
    ],
    searchingAnimation: {
      animations: -1,
      animationsPerSecond: -1,
      searchingLayouts:[
        {
          name: 'Unknown',
          blockId: -1,
          subId: -1,
          signLayout: [
            'LINE 01',
            'LINE 02',
            'LINE 03',
            'LINE 04'
          ]
        }
      ]
    }
  };
  animation: SignLayout = {
    name: 'loading1',
    signLayout: [
      "",
      "server loads...",
      "o                ",
      ""
    ],
    blockId: 159,
    subId: 14
  };
  animationCount: number = 0;

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','sign');
  }

  ngOnInit() {
    setTimeout(() =>{
      this._cloud.getSignConfig(JSON.parse(this._es.isElectronApp ?localStorage.getItem('currentUser'): this.cookie.get('currentUser'))).subscribe((t) => {
        this.singLayoutConfig = JSON.parse( t['response']) as SignLayoutConfig;
        Observable.interval(1000/this.singLayoutConfig.searchingAnimation.animationsPerSecond).subscribe(() =>{
          if(this.animationCount >= this.singLayoutConfig.searchingAnimation.animations){
            this.animationCount = 0;
          }
          this.animation = this.singLayoutConfig.searchingAnimation.searchingLayouts[this.animationCount];
          this.animationCount++;
        });
      }, error => {
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });
      this._cloud.getRandomServer(JSON.parse(this._es.isElectronApp ?localStorage.getItem('currentUser'): this.cookie.get('currentUser'))).subscribe((t) =>{
        this.server = (JSON.parse(t['response'])) as MinecraftServer;
      }, error => {
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });

    },200);

  }
  replaceColor(value: string) {

    return value
      .replace(/&4/g, '<span style="color: #AA0000; text-shadow: 1px 1px 0 #2A0000;" >')
      .replace(/&c/g, '<span style="color: #FF5555; text-shadow: 1px 1px 0 #3F1515;" >')
      .replace(/&6/g, '<span style="color: #FFAA00; text-shadow: 1px 1px 0 #2A2A00;" >')
      .replace(/&e/g, '<span style="color: #FFFF55; text-shadow: 1px 1px 0 #3F3F15;" >')
      .replace(/&2/g, '<span style="color: #00AA00; text-shadow: 1px 1px 0 #002A00;" >')
      .replace(/&a/g, '<span style="color: #55FF55; text-shadow: 1px 1px 0 #153F15;" >')
      .replace(/&b/g, '<span style="color: #55FFFF; text-shadow: 1px 1px 0 #153F3F;" >')
      .replace(/&3/g, '<span style="color: #00AAAA; text-shadow: 1px 1px 0 #002A2A;" >')
      .replace(/&1/g, '<span style="color: #0000AA; text-shadow: 1px 1px 0 #00002A;" >')
      .replace(/&9/g, '<span style="color: #5555FF; text-shadow: 1px 1px 0 #15153F;" >')
      .replace(/&d/g, '<span style="color: #FF55FF; text-shadow: 1px 1px 0 #3F153F;" >')
      .replace(/&5/g, '<span style="color: #AA00AA; text-shadow: 1px 1px 0 #2A002A;" >')
      .replace(/&7/g, '<span style="color: #AAAAAA; text-shadow: 1px 1px 0 #2A2A2A;" >')
      .replace(/&8/g, '<span style="color: #555555; text-shadow: 1px 1px 0 #151515;" >')
      .replace(/&0/g, '<span style="color: #000000; text-shadow: 1px 1px 0 #000000;" >')
      .replace(/&f/g, '<span style="color: #FFFFFF; text-shadow: 1px 1px 0 #3F3F3F;" >')

      .replace(/&l/g, '<span style="font-weight: bold">')
      .replace(/&n/g, '<span style="text-decoration: underline">')
      .replace(/&o/g, '<span style="font-style: italic;">')
      .replace(/&m/g, '<span style="text-decoration: line-through">')
      .replace(/&r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/§4/g, '<span style="color: #fe0000">')
      .replace(/§c/g, '<span style="color: #ff4c52">')
      .replace(/§6/g, '<span style="color: #ff8e42">')
      .replace(/§e/g, '<span style="color: #ffd800">')
      .replace(/§2/g, '<span style="color: #31a310">')
      .replace(/§a/g, '<span style="color: #4dd800">')
      .replace(/§b/g, '<span style="color: #01ffff">')
      .replace(/§3/g, '<span style="color: #0094fe">')
      .replace(/§1/g, '<span style="color: #0026ff">')
      .replace(/§9/g, '<span style="color: #4368ff">')
      .replace(/§d/g, '<span style="color: #fe00dc">')
      .replace(/§5/g, '<span style="color: #b100fe">')
      .replace(/§7/g, '<span style="color: #a0a0a0">')
      .replace(/§8/g, '<span style="color: #404040">')
      .replace(/§0/g, '<span style="color: #010101">')
      .replace(/§f/g, '<span style="color: #ffffff">')

      .replace(/§l/g, '<span style="font-weight: bold">')
      .replace(/§n/g, '<span style="text-decoration: underline">')
      .replace(/§o/g, '<span style="font-style: italic;">')
      .replace(/§m/g, '<span style="text-decoration: line-through">')
      .replace(/§r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/%server%/g, this.server.serverInfo.serviceId.serverId)
      .replace(/%state%/g, this.server.serverInfo.serverState)
      .replace(/%online_players%/g, this.server.serverInfo.onlineCount.toString())
      .replace(/%max_players%/g, this.server.serverInfo.maxPlayers.toString())
      .replace(/%motd%/g, this.server.serverInfo.motd)
      .replace(/%id%/g, this.server.serverInfo.serviceId.id.toString())
      .replace(/%host%/g, this.server.serverInfo.host)
      .replace(/%port%/g, this.server.serverInfo.port.toString())
      .replace(/%memory%/g, this.server.serverInfo.memory + 'MB')
      .replace(/%wrapper%/g, this.server.serverInfo.serviceId.wrapperId)
      .replace(/%extra%/g, String(this.server.serverInfo.serverConfig.extra))
      .replace(/%template%/g, this.server.serverInfo.template.name)
      .replace(/%group%/g, this.server.serverInfo.serviceId.group);
  }

  openEdit(layout: SignLayout) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('sign','edit');
    this.dialog.open(SignEditComponent, {
      data: layout,
    });
  }

  SaveConfig() {

    this._cloud.postSignConfig(JSON.parse(this._es.isElectronApp ?localStorage.getItem('currentUser'): this.cookie.get('currentUser')),this.singLayoutConfig).subscribe(()=>{
        this._notify.UpdateSuccesfully();
      }, error => {
          if ((error.status === 403)) {
            this._notify.AccessDenied();
          }
        });
  }

  AddSignGroup() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('sign','addgroup');
    this.dialog.open(GroupSignCreateComponent,{
      data: this.singLayoutConfig
    });
  }

  RemoveGroup(i: number) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('sign','removegroup');
    if(this.singLayoutConfig.groupLayouts.length !== 1){
      this.singLayoutConfig.groupLayouts.splice(i,1);
    }else{
      this._notify.LastItemError();
    }
  }

  EditSearch() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('sign','edit_search');
    this.dialog.open(EditSearchAnimationComponent,{
      data: this.singLayoutConfig
    });
  }
}

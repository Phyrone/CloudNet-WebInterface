/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatSort, MatTabChangeEvent, MatTableDataSource} from '@angular/material';
import {Observable, Subscription} from 'rxjs';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {ServerScreenComponent} from '../../popup/server-screen/server-screen.component';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {ServerInfo} from '../../_models/server-info';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-server-screening',
  templateUrl: './server-list.component.html',
  styleUrls: ['./server-list.component.css']
})
export class ServerListComponent implements OnInit {
  displayedColumns: string[] = ['serviceId', 'hostname', 'port','serverstate','onlinecount', 'actions'];
  isLoadingResults = true;
  groups: string[];
  groupservers: MatTableDataSource<ServerInfo> | null;

  @ViewChild(MatSort) sort: MatSort;
  private refresh: Subscription;

  constructor(public _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','server_list');
  }

  ngOnInit() {

    setTimeout(() => {
      this._cloud.getMinecraftServerGroups(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
        this.groups = t['response'] as string[];
        this.isLoadingResults = false;
      }, error1 => {
        if (error1.status == 403) {
          this.isLoadingResults = false;
          this._notify.AccessDenied();
        }
      });
    }, 200);
  }

  changeTab($event: MatTabChangeEvent) {
    this.isLoadingResults = true;
    let group: string = this.groups[$event.index];
    if (this.refresh != null) {
      this.refresh.unsubscribe();
    }

    this.refresh = Observable.interval(parseInt(localStorage.getItem('console.interval'))).subscribe(() => {
      this._cloud.getMinecraftServers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), group).subscribe(t => {
        let items: Array<ServerInfo> = [];
        for (let user of t['response']) {
          let item: ServerInfo = JSON.parse(user) as ServerInfo;
          items.push(item);
        }
        this.isLoadingResults = false;
        this.groupservers = new MatTableDataSource(items);
        this.groupservers.sort = this.sort;

      }, error1 => {
        if (error1.status == 403) {
          this._notify.AccessDenied();
          this.refresh.unsubscribe();
          this.isLoadingResults = false;
        }
      });
    });
  }

  openConsole(element: ServerInfo) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('server_list','console');
    this.dialog.open(ServerScreenComponent, {
      data: element
    });
  }

}

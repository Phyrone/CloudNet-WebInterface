/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef, MatSnackBar} from '@angular/material';
import {ServerGroup} from '../../_models/server-group';
import {ServerGroupType} from '../../_models/server-group-type.enum';
import {ServerGroupMode} from '../../_models/server-group-mode.enum';
import {PluginResourceType} from '../../_models/plugin-resource-type.enum';
import {TemplateResource} from '../../_models/template-resource';
import {ServerInstallablePlugin} from '../../_models/server-installable-plugin';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Template} from '../../_models/template';
import {PriorityService} from '../../_models/priority-service';
import {NotificationsService} from '../../_services/notifications.service';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-servergroups',
  templateUrl: './servergroups.component.html',
  styleUrls: ['./servergroups.component.css']
})
export class ServergroupsComponent implements OnInit {
  isLoadingResults = true;
  isRateLimitReached = false;
  items: ServerGroup[];

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, private _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','proxy_list');
  }

  ngOnInit() {
    this.items = [];
    this.refreshItems();
  }

  Edit(name: string) {
    this._cloud.getServer(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name).subscribe(t => {
      if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('servergroups','edit');
      let dialog = this.dialog.open(ServerEditDialog, {
        data: JSON.parse(t.response[name]) as ServerGroup,
      });
      dialog.afterClosed().subscribe(() => {
        this.refreshItems();
      });
    });

  }

  Manage(name: string) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('servergroups','manage');
    let dialog = this.dialog.open(ServerManageDialog, {
      data: name,
    });

    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });

  }

  Remove(name: string) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('servergroups','remove');
    if ((this.items.length === 1)) {
      this._notify.LastItemError();
      return;
    }
    this.dialog.open(ServerDeleteDialog, {
      data: name,
    });
    this.refreshItems();
  }

  Add() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('servergroups','add');
    let dialog = this.dialog.open(ServerAddDialog);
    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  Templates(name: string) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('servergroups','templates');
    this._cloud.getServer(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name).subscribe(t => {
      let dialog = this.dialog.open(ServerTemplateDialog, {
        data: JSON.parse(t.response[name]) as ServerGroup,
      });
      dialog.afterClosed().subscribe(() => {
        this.refreshItems();
      });
    });
  }

  refreshItems() {
    this.items = [];
    setTimeout(() => {
      this._cloud.getServer(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), null).subscribe((date) => {
        for (let server of date['response']) {
          let item: ServerGroup = JSON.parse(server) as ServerGroup;
          this.items.push(item);
        }
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
      }, error => {
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });
    }, 200);
  }
}

@Component({
  selector: 'servergroup-template-dialog',
  templateUrl: 'servergroups.component.templates.html',
})
export class ServerTemplateDialog implements OnInit {
  AddTemplateGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<ServerTemplateDialog>, @Inject(MAT_DIALOG_DATA) public group: ServerGroup,
              public _cloud: CloudConnectorService, private _formBuilder: FormBuilder, private _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  ngOnInit(): void {
    this.AddTemplateGroup = this._formBuilder.group({
      TemplateNameCtrl: ['', Validators.required],
      TemplateUrlCtrl: ['', Validators.nullValidator],
      TemplateBackendCtrl: ['', Validators.nullValidator],
    });
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  onSaveClick() {
    this._cloud.postSaveServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.group).subscribe(() => {
      this.dialogRef.close(true);
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
        this.dialogRef.close(false);
      }
    });
  }

  AddTemplate(name: string, url: string, value: string): void {
    let backend: TemplateResource = TemplateResource[value];
    let temp: Template = {name: name, url: url, backend: backend, processPreParameters: [], intallablePlugins: []};
    this.group.templates.push(temp);
  }


  SaveTemplateName(value: string, j: number) {
    this.group.templates[j].name = value;
  }

  SaveTemplateUrl(value: string, j: number) {
    this.group.templates[j].url = value;
  }

  SaveTemplateBackend(value: string, j: number) {
    this.group.templates[j].backend = TemplateResource[value];
  }

  AddPlugin(value1: string, value2: string, value3: string, j: number) {
    let plugin: ServerInstallablePlugin = {name: value1, pluginResourceType: PluginResourceType[value3], url: value2};
    let plugins: Array<ServerInstallablePlugin> = this.group.templates[j].intallablePlugins;
    if (plugins == null) {
      plugins = [];
    }
    plugins.push(plugin);
    this.group.templates[j].intallablePlugins = plugins;
  }

  SavePluginNameT(value: string, i: number, j: number) {
    this.group.templates[j].intallablePlugins[i].name = value;
  }

  SavePluginUrlT(value: string, i: number, j: number) {
    this.group.templates[j].intallablePlugins[i].url = value;
  }

  SavePluginResourceTypeT(value: string, i: number, j: number) {
    this.group.templates[j].intallablePlugins[i].pluginResourceType = PluginResourceType[value];
  }

  RemovePlugin(i: number, j: number) {
    this.group.templates[j].intallablePlugins.splice(i, 1);
  }

  RemoveTemplate(j: number) {
    if (this.group.templates.length == 1) {
      this._notify.LastItemError();
      return;
    }
    this.group.templates.splice(j, 1);
  }
}

@Component({
  selector: 'servergroup-delete-dialog',
  templateUrl: 'servergroups.component.delete.html',
})
export class ServerDeleteDialog {

  constructor(public dialogRef: MatDialogRef<ServerDeleteDialog>, @Inject(MAT_DIALOG_DATA) public name: string,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onDeleteClick(value: string) {
    this._cloud.postDeleteServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), value).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close();

  }
}

@Component({
  selector: 'servergroup-edit-dialog',
  templateUrl: 'servergroups.component.edit.html',
})
export class ServerEditDialog {
  WrapperControl = new FormControl();
  filteredWrappers: Observable<string[]>;
  wrappers: string[] = [];

  constructor(public dialogRef: MatDialogRef<ServerEditDialog>, @Inject(MAT_DIALOG_DATA) public servergroup: ServerGroup,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
    this._cloud.getWrappers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.wrappers = t['response'];
    });
    this.filteredWrappers = this.WrapperControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onSaveClick(): void {
    this._cloud.postSaveServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.servergroup).subscribe(() => {
      this.dialogRef.close();
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
        this.dialogRef.close();
      }
    });
  }

  SaveName(value: string) {
    this.servergroup.name = value;
  }

  SaveMemory(value: number) {
    this.servergroup.memory = value;
  }

  SaveDynamicMemory(value: number) {
    this.servergroup.dynamicMemory = value;
  }

  SaveJoinPower(value: number) {
    this.servergroup.joinPower = value;
  }

  SaveMinOnlineServer(value: number) {
    this.servergroup.minOnlineServers = value;
  }

  SaveMaxOnlineServer(value: number) {
    this.servergroup.maxOnlineServers = value;
  }

  SavePFNSA(value: number) {
    this.servergroup.percentForNewServerAutomatically = value;
  }

  SaveServerType(value: string) {
    this.servergroup.serverType = ServerGroupType[value];
  }

  SaveGroupMode(value: string) {
    this.servergroup.groupMode = ServerGroupMode[value];
  }

  AddWrapper(name: string) {
    this.servergroup.wrapper.push(name);
  }

  RemoveWrapper(name: string) {
    if (this.servergroup.wrapper.length === 1) {
      return;
    }
    const index: number = this.servergroup.wrapper.indexOf(name);
    if (index !== -1) {
      this.servergroup.wrapper.splice(index, 1);
    }
  }

  saveTemplateName(value: string) {
    this.servergroup.globalTemplate.name = value;
  }

  saveTemplateUrl(value: string) {
    this.servergroup.globalTemplate.url = value;
  }

  saveTemplateBackend(value: string) {
    this.servergroup.globalTemplate.backend = TemplateResource[value];
  }

  addPlugin(name: string, url: string, type: string) {
    let plugin: ServerInstallablePlugin = {name: name, pluginResourceType: PluginResourceType[type], url: url};
    let plugins: Array<ServerInstallablePlugin> = this.servergroup.globalTemplate.intallablePlugins;
    if (plugins == null) {
      plugins = [];
    }
    plugins.push(plugin);
    this.servergroup.globalTemplate.intallablePlugins = plugins;
  }

  savePluginName(value: string, int: number) {
    this.servergroup.globalTemplate.intallablePlugins[int].name = value;
  }

  savePluginUrl(value: string, int: number) {
    this.servergroup.globalTemplate.intallablePlugins[int].url = value;
  }

  savePluginResourceType(value: string, int: number) {
    this.servergroup.globalTemplate.intallablePlugins[int].pluginResourceType = PluginResourceType[value];
  }

  removePlugin(value: number) {
    this.servergroup.globalTemplate.intallablePlugins.splice(value, 1);
  }

  SaveStopTimeInSeconds(value: number) {
    this.servergroup.priorityService.stopTimeInSeconds = value;
  }

  saveGlobal(value1: number, value2: number) {
    this.servergroup.priorityService.global.onlineServers = value1;
    this.servergroup.priorityService.global.onlineCount = value2;
  }

  saveGroup(value1: number, value2: number) {
    this.servergroup.priorityService.group.onlineServers = value1;
    this.servergroup.priorityService.group.onlineCount = value2;
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.wrappers.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}

@Component({
  selector: 'servergroup-manage-dialog',
  templateUrl: 'servergroups.component.manage.html',
})
export class ServerManageDialog {

  constructor(public dialogRef: MatDialogRef<ServerManageDialog>, @Inject(MAT_DIALOG_DATA) public data: string,
              public _cloud: CloudConnectorService, private _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onStartClick(name: string, count: string) {
    this._cloud.postStartServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name, parseInt(count)).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
        this.dialogRef.close();
      }
    });
  }

  onShutdownClick(name: string): void {
    this._cloud.postShutdownServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
        this.dialogRef.close();
      }
    });
  }
}

@Component({
  selector: 'servergroup-Add-dialog',
  templateUrl: 'servergroups.component.add.html',
})
export class ServerAddDialog implements OnInit {
  filteredWrappers: Observable<string[]>;
  wrappers: string[] = [];
  NameFormGroup: FormGroup;
  MemoryFormGroup: FormGroup;
  StartUpFromGroup: FormGroup;
  AutoStartFromGroup: FormGroup;
  GroupModeCrtl = new FormControl('', [Validators.required]);
  ServerTypeCrtl = new FormControl('', [Validators.required]);

  TemplateBackendFromGroup: FormGroup;
  HunderPlayerFromGroup: FormGroup;
  HunderGlobalPlayerFromGroup: FormGroup;
  WrapperFromGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<ServerEditDialog>, public _cloud: CloudConnectorService,
              public snackBar: MatSnackBar, private _formBuilder: FormBuilder,
              private cookie: CookieService,private _es: ElectronService) {

  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  Create(): void {
    let Name: string = this.NameFormGroup.controls['NameCrtl'].value;
    let memory: number = this.MemoryFormGroup.controls['MemoryCrtl'].value;
    let StartUP: number = this.StartUpFromGroup.controls['StartUpCrtl'].value;
    let AutoStart: number = this.AutoStartFromGroup.controls['AutoStartCrtl'].value;
    let mode: string = this.GroupModeCrtl.value;
    let ServerType: string = this.ServerTypeCrtl.value;
    let Backend: string = this.TemplateBackendFromGroup.controls['TemplateBackendCrtl'].value;
    let hundert: number = this.HunderPlayerFromGroup.controls['HunderPlayerCrtl'].value;
    let hundertglobal: number = this.HunderGlobalPlayerFromGroup.controls['HunderGlobalPlayerCrtl'].value;
    let wrapper: string = this.WrapperFromGroup.controls['WrapperCrtl'].value;
    let service: PriorityService = {
      stopTimeInSeconds: 180,
      global: {onlineServers: hundertglobal, onlineCount: 100},
      group: {onlineServers: hundert, onlineCount: 100}
    };
    let template: Template = {
      intallablePlugins: [],
      processPreParameters: [],
      backend: TemplateResource[Backend],
      url: 'null',
      name: 'default'
    };
    let templateG: Template = {
      intallablePlugins: [],
      processPreParameters: [],
      backend: TemplateResource.LOCAL,
      url: 'null',
      name: 'globaltemplate'
    };
    let group: ServerGroup = {
      name: Name,
      wrapper: wrapper.split(','),
      kickedForceFallback: (mode == 'LOBBY') as boolean,
      memory: memory,
      dynamicMemory: memory,
      joinPower: 0,
      maintenance: true,
      minOnlineServers: StartUP,
      maxOnlineServers: -1,
      serverType: ServerGroupType[ServerType],
      priorityService: service,
      percentForNewServerAutomatically: AutoStart,
      templates: [template],
      globalTemplate: templateG,
      groupMode: ServerGroupMode[mode],
      advancedServerConfig: {
        notifyPlayerUpdatesFromNoCurrentPlayer: false,
        notifyProxyUpdates: false,
        notifyServerUpdates: false,
        disableAutoSavingForWorlds: (mode != 'STATIC')
      }
    };
    this._cloud.postSaveServerGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), group).subscribe(() => {
      this.snackBar.open('Succefully created', 'Ok', {duration: 2000});
      this.dialogRef.close(true);
    }, error => {
      if ((error.status === 403)) {
        this.snackBar.open('You don\'t have Permissions for this', 'I love Errors', {duration: 2000});
        this.dialogRef.close(false);
      }
    });
  }

  ngOnInit(): void {
    this.NameFormGroup = this._formBuilder.group({
      NameCrtl: ['', Validators.required]
    });
    this.MemoryFormGroup = this._formBuilder.group({
      MemoryCrtl: ['', Validators.required]
    });
    this.StartUpFromGroup = this._formBuilder.group({
      StartUpCrtl: ['', Validators.required]
    });
    this.AutoStartFromGroup = this._formBuilder.group({
      AutoStartCrtl: ['', Validators.required]
    });
    this.TemplateBackendFromGroup = this._formBuilder.group({
      TemplateBackendCrtl: ['', Validators.required]
    });
    this.HunderPlayerFromGroup = this._formBuilder.group({
      HunderPlayerCrtl: ['', Validators.required]
    });
    this.HunderGlobalPlayerFromGroup = this._formBuilder.group({
      HunderGlobalPlayerCrtl: ['', Validators.required]
    });
    this.WrapperFromGroup = this._formBuilder.group({
      WrapperCrtl: ['', Validators.required]
    });
    this._cloud.getWrappers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.wrappers = t['response'];
    });
    this.filteredWrappers = this.WrapperFromGroup.controls['WrapperCrtl'].valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.wrappers.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {BaseChartDirective} from 'ng2-charts';
import {Observable, Subscription} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatomoTracker} from 'ngx-matomo';
import {AppConfigService} from '../../_services/app-config.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './my-dashboard.component.html',
  styleUrls: ['./my-dashboard.component.css']
})
export class MyDashboardComponent implements OnInit,OnDestroy {
  @ViewChild(BaseChartDirective) chart: BaseChartDirective;

  players: number = 0;
  servers: number = 0;
  groups: number = 0;
  proxys: number = 0;
  cloudStats = '';
  public lineChartData: Array<any> = [{data: [], label: 'Players'}];
  public lineChartLabels:Array<any> = [];
  public lineChartType:string = 'line';
  public lineChartLegend:boolean = true;
  public lineChartOptions:any = {
    responsive: true,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero:true,
          stepSize: 1
        }
      }]
    }
  };
  public lineChartColors:Array<any> = [{
      backgroundColor: '#2196f3',
      borderColor: '#2196f3'
    }];
  private sub: Subscription;

  constructor(private _cloud: CloudConnectorService, private _t: TranslateService,private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','dashboard');
  }

  static getFormattedTime(){
    let d:Date = new Date();
    return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
  }

  ngOnInit() {
    this._t.get('page.dashboard.text.players').subscribe(t => {
      this.lineChartData = [{data: [], label: t}];
    });

    this.sub = Observable.interval(parseInt(localStorage.getItem('dashboard.interval'))).subscribe(() => {
        this._cloud.getCloudStats(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((data)=>{
          this.cloudStats = data['response'];
        });
        this._cloud.getPlayerCount(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((date) => {
          this.players = date['response'] as number;
          if ((this.lineChartData[0]['data'] as Array<any>).length > 50) {
            this.lineChartData[0]['data'] = (this.lineChartData[0]['data'] as Array<any>).slice(0, 15);
            this.lineChartLabels = this.lineChartLabels.slice(0, 15);
          }
          this.lineChartData[0]['data'].push(date['response'] as number);
          this.lineChartLabels.push(MyDashboardComponent.getFormattedTime());
          this.chart.chart.update();

        });
        this._cloud.getServerCount(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((date) => {
          this.servers = date['response'] as number;
        });
        this._cloud.getGroupCount(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((date) => {
          this.groups = date['response'] as number;
        });
        this._cloud.getProxyCount(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((date) => {
          this.proxys = date['response'] as number;
        });

      }
    );
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
}

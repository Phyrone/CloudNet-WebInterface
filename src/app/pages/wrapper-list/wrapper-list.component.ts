/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {Wrapper} from '../../_models/wrapper';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatDialog, MatTableDataSource} from '@angular/material';
import {Observable, Subscription} from 'rxjs';
import {WrapperDetailViewComponent} from '../../popup/wrapper-detail-view/wrapper-detail-view.component';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-wrapper-list',
  templateUrl: './wrapper-list.component.html',
  styleUrls: ['./wrapper-list.component.css']
})
export class WrapperListComponent implements OnInit {
  displayedColumns: string[] = ['serverId', 'usedMemory', 'cpuUsage', 'proxy','hostname','queue', 'servers', 'user', 'view' ];
  dataSource: MatTableDataSource<Wrapper> | null = new MatTableDataSource([] as Wrapper[]);
  isLoadingResults = true;
  private refresh: Subscription;
  constructor(private _cloud: CloudConnectorService,
              private cookie: CookieService,
              private _es: ElectronService,
              private dialog: MatDialog,
              private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','wrapper-list');
  }

  ngOnInit() {
    this.refresh = Observable.interval(parseInt(localStorage.getItem('console.interval'))).subscribe(() => {
      this._cloud.getWrapperInfos(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(value => {
        let items: Array<Wrapper> = [];
        for (let user of value['response']) {
          let item: Wrapper = JSON.parse(user) as Wrapper;
          items.push(item);
        }
        this.dataSource = new MatTableDataSource( items);
        this.isLoadingResults = false;
      });
    });

  }

  openView(element: Wrapper) {
      this.dialog.open(WrapperDetailViewComponent, {
      data: element,
      hasBackdrop: false
    });
  }
}

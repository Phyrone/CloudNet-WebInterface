/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {PermissionGroup} from '../../../_models/permission-group';
import {FormControl} from '@angular/forms';
import {CloudConnectorService} from '../../../_services/cloud-connector.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NotificationsService} from '../../../_services/notifications.service';
import {Permission} from '../../../_models/permission';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-add-group',
  templateUrl: './add-group.component.html',
  styleUrls: ['./add-group.component.css']
})
export class AddGroupComponent implements OnInit {

  public group: PermissionGroup;
  public permissions: IterableIterator<string>;
  private name: string;
  implementGroup =  new FormControl();
  public groups: string[];

  constructor(private _cloud: CloudConnectorService, private route: ActivatedRoute, private _notify: NotificationsService,
              private router: Router,private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','permissiongroup');
    this._cloud.getGroups(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.groups = t['response'];
    });
  }

  ngOnInit() {
    this.group = new PermissionGroup();
    this.group.defaultGroup = false;
    this.group.implementGroups = [];
    this.group.permissions = [];
    this.group.serverGroupPermissions = [];
    this.group.options = [];
    if (this.group.name === undefined) {
      this.group.name = null;
    }
    if (this.group.prefix === undefined) {
      this.group.prefix = null;
    }
    if (this.group.suffix === undefined) {
      this.group.suffix = null;
    }
    if (this.group.display === undefined) {
      this.group.display = null;
    }
  }

  SavePrefix(name: string) {
    this.group.prefix = name;
  }
  SaveSuffix(name: string) {
    this.group.suffix = name;
  }
  SaveDisplay(name: string) {
    this.group.display = name;
  }
  SavePower(name: string) {
    this.group.joinPower = parseInt(name);
  }
  SaveID(name: string) {
    this.group.tagId = parseInt(name);
  }
  SubmitGroup() {
    this._cloud.updatePermissionGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.group).subscribe( () => {
      this._notify.UpdateSuccesfully();
    }, error1 => {
      if ((error1.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }


  AddPermissionToGroup(permission: string) {
    if (permission.startsWith('-')) {
      this.group.permissions.push({key: permission, value: false});
    } else {
      this.group.permissions.push({key: permission, value: true});
    }
  }

  RemovePermissionFromGroup(permission: Permission) {
    if (this.group.permissions.length <= 0) {
      return;
    }
    const index: number = this.group.permissions.indexOf(permission);
    if (index !== -1) {
      this.group.permissions.splice(index, 1);
    }
  }

  AddPermissionToServerGroup(i: number, value: string) {
    this.group.serverGroupPermissions[i].value.push(value);
  }

  AddServerGroup(value: string) {
    if (this.group.serverGroupPermissions.filter(t => t.key === value).length !== 1) {
      this.group.serverGroupPermissions.push({key: value, value: []});
    }
  }

  RemovePermissionFromServerGroup(i: number, permission: string) {
    const index: number = this.group.serverGroupPermissions[i].value.indexOf(permission);
    if (index !== -1) {
      this.group.serverGroupPermissions[i].value.splice(index, 1);
    }
  }

  RemoveServerGroup(key: string) {
    if (this.group.serverGroupPermissions.findIndex(t => t.key === key) !== 1) {
      this.group.serverGroupPermissions.splice(this.group.serverGroupPermissions.findIndex(t => t.key === key), 1);
    }
  }

  AddGroup(value: string) {
    this.group.implementGroups.push(value);
  }

  RemoveGroup(item: string) {
    if (this.group.implementGroups.length <= 0) {
      return;
    }
    const index: number = this.group.implementGroups.indexOf(item);
    if (index !== -1) {
      this.group.implementGroups.splice(index, 1);
    }
  }

  onSaveName(value: string) {
    this.group.name = value;
  }


}

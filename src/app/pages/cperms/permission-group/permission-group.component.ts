/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnDestroy, OnInit} from '@angular/core';
import {CloudConnectorService} from '../../../_services/cloud-connector.service';
import {ActivatedRoute, Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {PermissionGroup} from '../../../_models/permission-group';
import {Permission} from '../../../_models/permission';
import {NotificationsService} from '../../../_services/notifications.service';
import {FormControl} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-permission-group',
  templateUrl: './permission-group.component.html',
  styleUrls: ['./permission-group.component.css']
})
export class PermissionGroupComponent implements OnInit, OnDestroy {

  public group: PermissionGroup;
  public permissions: IterableIterator<string>;
  private name: string;
  private sub: Subscription;
  implementGroup =  new FormControl();
  public groups: string[];

  constructor(private _cloud: CloudConnectorService, private route: ActivatedRoute, private _notify: NotificationsService,
              private router: Router,private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    this._cloud.getGroups(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.groups = t['response'];
    });
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','permissiongroup');
  }

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.name = params['group']; // (+) converts string 'id' to a number
    });
    setTimeout(() => {
      this._cloud.getPermissionGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.name).subscribe(t => {
        this.group = JSON.parse(t['response']) as PermissionGroup;
      }, error1 => {
        if ((error1.status === 403)) {
          this._notify.AccessDenied();
        }
      });
    }, 500);
  }

  ngOnDestroy(): void {
    this.sub.unsubscribe();
  }
  SavePrefix(name: string) {
    this.group.prefix = name;
  }
  SaveSuffix(name: string) {
    this.group.suffix = name;
  }
  SaveDisplay(name: string) {
    this.group.display = name;
  }
  SavePower(name: string) {
    this.group.joinPower = parseInt(name);
  }
  SaveID(name: string) {
    this.group.tagId = parseInt(name);
  }
  SubmitGroup() {
    this._cloud.updatePermissionGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.group).subscribe( () => {
        this._notify.UpdateSuccesfully();
    }, error1 => {
      if ((error1.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }


  AddPermissionToGroup(permission: string) {
    if (permission.startsWith('-')) {
      this.group.permissions.push({key: permission, value: false});
    } else {
      this.group.permissions.push({key: permission, value: true});
    }
  }

  RemovePermissionFromGroup(permission: Permission) {
    if (this.group.permissions.length <= 0) {
      return;
    }
    const index: number = this.group.permissions.indexOf(permission);
    if (index !== -1) {
      this.group.permissions.splice(index, 1);
    }
  }

  AddPermissionToServerGroup(i: number, value: string) {
    this.group.serverGroupPermissions[i].value.push(value);
  }

  AddServerGroup(value: string) {
    if (this.group.serverGroupPermissions.filter(t => t.key === value).length !== 1) {
      this.group.serverGroupPermissions.push({key: value, value: []});
    }
  }

  RemovePermissionFromServerGroup(i: number, permission: string) {
    const index: number = this.group.serverGroupPermissions[i].value.indexOf(permission);
    if (index !== -1) {
      this.group.serverGroupPermissions[i].value.splice(index, 1);
    }
  }

  RemoveServerGroup(key: string) {
    if (this.group.serverGroupPermissions.findIndex(t => t.key === key) !== 1) {
      this.group.serverGroupPermissions.splice(this.group.serverGroupPermissions.findIndex(t => t.key === key), 1);
    }
  }

  AddGroup(value: string) {
    this.group.implementGroups.push(value);
  }

  RemoveGroup(item: string) {
    if (this.group.implementGroups.length <= 0) {
      return;
    }
    const index: number = this.group.implementGroups.indexOf(item);
    if (index !== -1) {
      this.group.implementGroups.splice(index, 1);
    }
  }

  onSaveName(value: string) {
    this.group.name = value;
  }

  onDeleteGroup() {
    this._cloud.postPermissionGroupDelete(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.group.name).subscribe( () => {
      this._notify.DeleteSuccessfully();
      this.router.navigate(['/cperms']).catch();
    }, error1 => {
      if ((error1.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }
}

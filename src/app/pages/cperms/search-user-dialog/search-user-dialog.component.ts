/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit, Renderer2} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {MatDialog, MatDialogRef} from '@angular/material';
import {CloudConnectorService} from '../../../_services/cloud-connector.service';
import {NotificationsService} from '../../../_services/notifications.service';
import {Router} from '@angular/router';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';


@Component({
  selector: 'app-search-user-dialog',
  templateUrl: './search-user-dialog.component.html',
  styleUrls: ['./search-user-dialog.component.css']
})
export class SearchUserDialogComponent implements OnInit {

  search = new FormGroup({
    player: new FormControl()
  });
  constructor(public dialogRef: MatDialogRef<SearchUserDialogComponent>, public _cloud: CloudConnectorService, private renderer: Renderer2,
              private _notify: NotificationsService, public dialog: MatDialog, private router: Router,
              private cookie: CookieService,
              private _es: ElectronService,
              private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','cperms_serachuser');
  }

  ngOnInit() {
  }



  onSearch() {
    const player = this.Player.value;
    this._cloud.getPermissionUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), player).subscribe(() => {
      this.dialogRef.close();
      this.router.navigate(['/cperms/edituser', player]).catch();
    }, error => {
      if (error.status === 403) {
        this._notify.AccessDenied();
        this.dialogRef.close();
      }
      if (error.status === 400) {
        this._notify.PlayerNotFoundError();
      }
    });
  }
  onClose() {
    this.dialogRef.close();
  }

  get Player() { return this.search.get('player'); }


}

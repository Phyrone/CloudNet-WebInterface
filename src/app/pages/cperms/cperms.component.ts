/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit} from '@angular/core';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-cperms',
  templateUrl: './cperms.component.html',
  styleUrls: ['./cperms.component.css']
})
export class CpermsComponent implements OnInit {


  constructor(private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','cperms');
  }

  ngOnInit() {
  }

}



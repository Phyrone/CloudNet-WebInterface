/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {CloudConnectorService} from '../../../_services/cloud-connector.service';
import {ActivatedRoute} from '@angular/router';
import {OfflinePlayer} from '../../../_models/offline-player';
import {FormControl, FormGroup} from '@angular/forms';
import {NotificationsService} from '../../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';


@Component({
  selector: 'app-permission-user',
  templateUrl: './permission-user.component.html',
  styleUrls: ['./permission-user.component.css'],

})
export class PermissionUserComponent implements OnInit {
  player: OfflinePlayer;
  user = new FormGroup({
    datePicker: new FormControl(),
    groupName: new FormControl(),
    permission: new FormControl()
  });

  constructor(public _cloud: CloudConnectorService, private route: ActivatedRoute, private _Notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,
              private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','permissionuser');
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this._cloud.getPermissionUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), params['user']).subscribe(t => {
        this.player = JSON.parse(t['response']) as OfflinePlayer;
        if (this.player.permissionEntity.prefix === undefined) {
          this.player.permissionEntity.prefix = null;
        }
        if (this.player.permissionEntity.suffix === undefined) {
          this.player.permissionEntity.suffix = null;
        }
      });
    });
  }

  AddGroupToUser() {
    const d = new Date(Date.parse(this.Date.value));
    const current = Date.now();
    if (d.getTime() - current < 0) {
      this.player.permissionEntity.groups.push({key: this.Group.value, value: 0});
    } else {
      this.player.permissionEntity.groups.push({key: this.Group.value, value: d.getTime()});
    }
    this.Date.reset();
    this.Group.reset();
  }

  SubmitUser() {
    this._cloud.updatePermissionUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.player).subscribe( () => {
      this._Notify.UpdateSuccesfully();
    }, error1 => {
      if ((error1.status === 403)) {
        this._Notify.AccessDenied();
      }
    });
  }

  onSavePrefix(value: string) {
    this.player.permissionEntity.prefix = value;
  }

  onSaveSuffix(value: string) {
    this.player.permissionEntity.suffix = value;
  }
  get Date() { return this.user.get('datePicker'); }
  get Group() { return this.user.get('groupName'); }
  get Permission() { return this.user.get('permission'); }

  onAddPermission() {
    const value: string = this.Permission.value;
    if (value.startsWith('-')) {
      this.player.permissionEntity.permissions.push( {key: value, value: false});
    } else {
      this.player.permissionEntity.permissions.push( {key: value, value: true});
    }
    this.Permission.reset();
  }

  onRemovePermission(item: string) {
    this.player.permissionEntity.permissions = this.player.permissionEntity.permissions.filter(t => !t.key.includes(item));
  }

  onRemoveGroupFromUser(key: string) {
    if (this.player.permissionEntity.groups.filter(t => !t.key.includes(key)).length === 0) {
      this._Notify.LastItemError();
      return;
    }
    this.player.permissionEntity.groups = this.player.permissionEntity.groups.filter(t => !t.key.includes(key));
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSort,  MatTableDataSource} from '@angular/material';
import {PermissionGroup} from '../../../_models/permission-group';
import {CloudConnectorService} from '../../../_services/cloud-connector.service';
import {SearchUserDialogComponent} from '../search-user-dialog/search-user-dialog.component';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatomoTracker} from 'ngx-matomo';
import {AppConfigService} from '../../../_services/app-config.service';

@Component({
  selector: 'app-permission-navigation',
  templateUrl: './permission-navigation.component.html',
  styleUrls: ['./permission-navigation.component.css']
})
export class PermissionNavigationComponent implements OnInit {
  public groups: MatTableDataSource<PermissionGroup>;
  displayedGroupsColumns: string[] = ['group', 'joinpower', 'tagid', 'edit'];

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) GroupPaginator: MatPaginator;

  constructor(public _cloud: CloudConnectorService, public dialog: MatDialog, private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','permission_navigation');
  }

  ngOnInit() {
    this.refreshGroups();
  }

  refreshGroups() {
    setTimeout(() => {
      this._cloud.getPermissionGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), null).subscribe(t => {
        const items: Array<PermissionGroup> = [];
        for (const group of t['response']) {
          items.push(JSON.parse(group) as PermissionGroup);
        }
        this.groups = new MatTableDataSource(items);
        this.groups.sort = this.sort;
        this.groups.paginator = this.GroupPaginator;

      });
    }, 200);
  }

  ApplyGroupsFilter(value: string) {
    this.groups.filter = value.trim().toLowerCase();
  }

  onSearchUser() {
    this.dialog.open(SearchUserDialogComponent,{
      hasBackdrop: false
    });
  }
}

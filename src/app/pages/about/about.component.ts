/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Component, OnInit } from '@angular/core';
import {UpdaterService} from '../../_services/updater.service';
import {User} from '../../_models/user';
import {CookieService} from 'ngx-cookie-service';
import {MatDialogRef} from '@angular/material';
import {BehaviorSubject} from 'rxjs';
import {ElectronService} from 'ngx-electron';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css']
})
export class AboutComponent implements OnInit {
  currentUser: BehaviorSubject<string> = new BehaviorSubject<string>('No User Found');
  version: BehaviorSubject<string> = new BehaviorSubject('FAKE-VERSION');
  cloudnetversion: BehaviorSubject<string> = new BehaviorSubject('No Cloud found');

  constructor(public _update: UpdaterService,
              private cookie: CookieService,
              public dialogRef: MatDialogRef<AboutComponent>,
              private _es: ElectronService,
              private _cloud: CloudConnectorService,
              private matomoTracker: MatomoTracker) {

    this._update.load();
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','about');
  }

  ngOnInit() {
    setTimeout(()=>{
      this.version.next(this._update.settings.version + '-' + this._update.settings.type.toString());
      this.currentUser.next((JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')) as User).name);
      this._cloud.getCloudNetVersion((JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')) as User))
        .subscribe(t=>this.cloudnetversion.next(t['response']));
      if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('information','cloudnet',this.cloudnetversion.getValue());
      if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('information','webinterface',this.version.getValue());
    },200);
  }

  onClose() {
    this.dialogRef.close();
  }
}

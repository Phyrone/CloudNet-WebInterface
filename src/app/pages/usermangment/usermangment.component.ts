/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit} from '@angular/core';
import {User} from '../../_models/user';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import 'rxjs-compat/add/observable/of';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatomoTracker} from 'ngx-matomo';
import {AppConfigService} from '../../_services/app-config.service';

@Component({
  selector: 'app-usermangment',
  templateUrl: './usermangment.component.html',
  styleUrls: ['./usermangment.component.css']
})
export class UsermangmentComponent implements OnInit {
  displayedColumns: string[] = ['name', 'button', 'delete'];
  data: User[] = [];
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','usermangement');
  }

  public PasswordReset(name: User) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('usermangement','password_rest');
    let dialog = this.dialog.open(UserPasswordDialog, {
      data: name.name
    });
    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  public EditPermission(name: User) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('usermangement','edit_permission');
    let dialog = this.dialog.open(EditPermissionDialog, {
      data: name,
    });
    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  public AddUser() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('usermangement','add_user');
    let dialog = this.dialog.open(AddUserDialog);
    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  public Delete(name: User) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('usermangement','delete');
    let dialog = this.dialog.open(DeleteUserDialog, {
      data: name,
    });
    dialog.afterClosed().subscribe(() => {
      this.refreshItems();
    });
  }

  ngOnInit() {
    this.refreshItems();

  }

  public refreshItems() {
    setTimeout(() => {
      this._cloud.getUserItems(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((date) => {
        let items: Array<User> = [];
        for (let user of date['response']) {
          let item: User = JSON.parse(user) as User;
          items.push(item);
        }
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        this.data = items;
      }, error => {
        if (error.status == 403) {
          this._notify.AccessDenied();
          this.isLoadingResults = false;
          this.isRateLimitReached = false;
        } else {
          this.isLoadingResults = false;
          this.isRateLimitReached = true;
        }
      });
    }, 500);
  }
}

@Component({
  selector: 'edit-permission-dialog',
  templateUrl: 'usermangment.component.permissions.html',
})
export class EditPermissionDialog {

  constructor(public dialogRef: MatDialogRef<EditPermissionDialog>, @Inject(MAT_DIALOG_DATA) public user: User,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  addPermission(permission: string) {
    let permissions: Array<string> = this.user.permissions;
    permissions.push(permission);
    this.user.permissions = permissions;
  }

  removePermission(permission: string) {
    let permissions: Array<string> = this.user.permissions;
    const index: number = permissions.indexOf(permission);
    if (index !== -1) {
      permissions.splice(index, 1);
    }
    this.user.permissions = permissions;
  }

  onSave(username: User) {
    this._cloud.postUpdateUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), username).subscribe(() => {
      this._notify.UpdateSuccesfully();
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close(true);
  }
}

@Component({
  selector: 'delete-user-dialog',
  templateUrl: 'usermangment.component.delete.html',
})
export class DeleteUserDialog {

  constructor(public dialogRef: MatDialogRef<DeleteUserDialog>, @Inject(MAT_DIALOG_DATA) public user: User,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  onDelete() {
    this._cloud.postDeleteUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.user.name).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close(true);
  }
}

@Component({
  selector: 'add-user-dialog',
  templateUrl: 'usermangment.component.add.html',
})
export class AddUserDialog {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<AddUserDialog>, public _cloud: CloudConnectorService, private _formBuilder: FormBuilder,
              public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  onCreate() {
    this._cloud.postAddUser(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.firstFormGroup.controls['firstCtrl'].value, this.secondFormGroup.controls['secondCtrl'].value).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close(true);
  }

}

@Component({
  selector: 'resetuserpassword-dialog',
  templateUrl: 'usermangment.component.resetpassword.html',
})
export class UserPasswordDialog {
  firstFormGroup: FormGroup;

  constructor(public dialogRef: MatDialogRef<UserPasswordDialog>, @Inject(MAT_DIALOG_DATA) public username: string,
              public _cloud: CloudConnectorService, public _notify: NotificationsService, private _formBuilder: FormBuilder,
              private cookie: CookieService,private _es: ElectronService) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  onChange(username: string) {
    this._cloud.postResetUserPassword(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), username, this.firstFormGroup.controls['firstCtrl'].value).subscribe(() => {
      this._notify.UpdateSuccesfully();
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close(true);
  }

}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, OnInit, ViewChild} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MatDialog, MatSort, MatTabChangeEvent, MatTableDataSource} from '@angular/material';
import {SimpleProxyGroup} from '../../_models/simple-proxy-group';
import {ProxyScreenComponent} from '../../popup/proxy-screen/proxy-screen.component';
import {Observable, Subscription} from 'rxjs';
import {NotificationsService} from '../../_services/notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {AppConfigService} from '../../_services/app-config.service';
import {MatomoTracker} from 'ngx-matomo';

@Component({
  selector: 'app-proxy-screening',
  templateUrl: './proxy-list.component.html',
  styleUrls: ['./proxy-list.component.css']
})
export class ProxyListComponent implements OnInit {
  displayedColumns: string[] = ['serviceId',  'online', 'hostname', 'port', 'onlinecount', 'actions'];
  isLoadingResults = true;
  groups: string[];
  groupservers: MatTableDataSource<SimpleProxyGroup> | null;

  @ViewChild(MatSort) sort: MatSort;
  private refresh: Subscription;

  constructor(public _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','proxy_list');
  }

  ngOnInit() {

    setTimeout(() => {
    this._cloud.getProxyGroups(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
        this.groups = t['response'] as string[];
      }, error1 => {
        if (error1.status === 403) {
          this.isLoadingResults = false;
          this._notify.AccessDenied();
        }
      });
    }, 200);
  }

  changeTab($event: MatTabChangeEvent) {
    this.isLoadingResults = true;
    const group: string = this.groups[$event.index];
    if (this.refresh != null) {
      this.refresh.unsubscribe();
    }

    this.refresh = Observable.interval(parseInt(localStorage.getItem('console.interval'))).subscribe(() => {
      this._cloud.getProxyServers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), group).subscribe(t => {
        const items: Array<SimpleProxyGroup> = [];
        for (const user of t['response']) {
          const item: SimpleProxyGroup = JSON.parse(user) as SimpleProxyGroup;
          items.push(item);
        }
        this.isLoadingResults = false;
        this.groupservers = new MatTableDataSource(items);
        this.groupservers.sort = this.sort;
      }, error1 => {
        if (error1.status === 403) {
          this.isLoadingResults = false;
          this._notify.AccessDenied();
          this.refresh.unsubscribe();
        }
      });
    });
  }

  openConsole(element: SimpleProxyGroup) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('proxy_list','console');
    this.dialog.open(ProxyScreenComponent, {
      data: element
    });
  }

}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, Inject, OnInit, ViewEncapsulation} from '@angular/core';
import {CloudConnectorService} from '../../_services/cloud-connector.service';
import {MAT_DIALOG_DATA, MatDialog, MatDialogRef} from '@angular/material';
import {ProxyGroup} from '../../_models/proxyGroup';
import {Motd} from '../../_models';
import {ProxyGroupMode} from '../../_models/proxy-group-mode.enum';
import {ProxyVersion} from '../../_models/proxy-version.enum';
import {ServerFallback} from '../../_models/server-fallback';
import {TemplateResource} from '../../_models/template-resource';
import {ServerInstallablePlugin} from '../../_models/server-installable-plugin';
import {PluginResourceType} from '../../_models/plugin-resource-type.enum';
import {ProxyConfig} from '../../_models/proxy-config';
import {NotificationsService} from '../../_services/notifications.service';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {MatomoTracker} from 'ngx-matomo';
import {AppConfigService} from '../../_services/app-config.service';

@Component({
  selector: 'app-proxygroups',
  templateUrl: './proxygroups.component.html',
  styleUrls: ['./proxygroups.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ProxygroupsComponent implements OnInit {
  items: Array<ProxyGroup> = [];
  isLoadingResults = true;
  isRateLimitReached = false;

  constructor(private _cloud: CloudConnectorService, public dialog: MatDialog, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService,
              private matomoTracker: MatomoTracker) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('page','disclaimer');
  }

  ngOnInit() {
    this.items = [];
    setTimeout(() => {
      this.refreshItems();
    }, 200);
  }

  edit(name: string) {
    this._cloud.getProxy(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name).subscribe(t => {

      if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('proxygroups','edit');
      const dialog = this.dialog.open(EditDialog, {
        data: JSON.parse(t.response[name]) as ProxyGroup,
      });
      dialog.afterClosed().subscribe(t => {
        if (t) {
          this.refreshItems();
        }
      });
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });

  }

  manage(name: string) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('proxygroups','manage');
    const dialog = this.dialog.open(ManageDialog, {
      data: name,
    });
    dialog.afterClosed().subscribe(t => {
      if (t) {
        this.refreshItems();
      }
    });
  }

  remove(name: string) {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('proxygroups','remove');
    if ((this.items.length === 1)) {
      this._notify.LastItemError();
      return;
    }
    const dialog = this.dialog.open(DeleteDialog, {
      data: name,
    });
    dialog.afterClosed().subscribe(t => {
      if (t) {
        this.refreshItems();
      }
    });
  }

  add() {
    const proxyconfig: ProxyConfig = {
      enabled: true,
      maintenance: false,
      motdsLayouts: [
        {
          firstLine: '   §b§lCloud§f§lNet§8■ §7your §bfree §7cloudsystem §8§l【§f%version%§8§l】',
          secondLine: '         §aOnline §8» §7We are now §aavailable §7for §ball'
        }
      ],
      maintenanceMotdLayout: {
        firstLine: '   §b§lCloud§f§lNet§8■ §7your §bfree §7cloudsystem §8§l【§f%version%§8§l】',
        secondLine: '         §bMaintenance §8» §7We are still in §bmaintenance'

      },
      maintenaceProtocol: '§8➜ §bMaintenance §8§l【§c✘§8§l】',
      maxPlayers: 1000,
      fastConnect: false,
      customPayloadFixer: false,
      autoSlot: {
        enabled: false, dynamicSlotSize: 0
      },
      tabList: {
        enabled: true,
        header: ' \n§b§lCloud§f§lNet §8× §7your §bfree §7cloudsystem §8➜ §f%online_players%§8/§f%max_players%§f\n §8► §7Current server §8● §b%server% §8◄ \n ',
        footer: ' \n §7Twitter §8» §f@Dytanic §8▎ §7Discord §8» §fdiscord.gg/UNQ4wET \n §7powered by §8» §b§b§lCloud§f§lNet \n '
      },
      playerInfo: [' ', '§b§lCloud§f§lNet §8× §7your §bfree §7cloudsystem', '§7Twitter §8» §f@CloudNetService', '§7Discord §8» §fdiscord.gg/UNQ4wET', ' '],
      whitelist: [],
      dynamicFallback: {
        defaultFallback: 'Lobby',
        fallbacks: [
          {group: 'Lobby', permission: 'NULL'}
        ]
      }
    };
    const proxygroup: ProxyGroup = {
      name: 'Bungee',
      wrapper: ['Wrapper-1'],
      proxyVersion: ProxyVersion.BUNGEECORD,
      template: {
        name: 'default',
        backend: TemplateResource.LOCAL,
        url: 'NULL',
        processPreParameters: [''],
        intallablePlugins: [{pluginResourceType: PluginResourceType.LOCAL, name: '', url: ''}]
      },
      startPort: 25565,
      startup: 1,
      memory: 256,
      proxyConfig: proxyconfig,
      proxyGroupMode: ProxyGroupMode.DYNAMIC
    };
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('proxygroups','add');
    const dialog = this.dialog.open(AddDialog, {
      data: proxygroup,
    });
    dialog.afterClosed().subscribe(t => {
      if (t) {
        this.refreshItems();
      }
    });
  }

  refreshItems() {
    this.items = [];
    setTimeout(() => {
      this._cloud.getProxy(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), null).subscribe((date) => {
        for (const proxy of date['response']) {
          this.items.push(JSON.parse(proxy) as ProxyGroup);
        }
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
      }, error => {
        this.isLoadingResults = false;
        this.isRateLimitReached = false;
        if ((error.status === 403)) {
          this._notify.AccessDenied();
        }
      });
    }, 200);
  }

}

@Component({
  selector: 'delete-dialog',
  templateUrl: 'proxygroups.component.delete.html',
})
export class DeleteDialog {

  constructor(public dialogRef: MatDialogRef<DeleteDialog>, @Inject(MAT_DIALOG_DATA) public name: string,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  ondDeleteClick(value: string) {
    this._cloud.postDeleteProxyGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), value).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
    this.dialogRef.close(true);

  }
}

@Component({
  selector: 'manage-dialog',
  templateUrl: 'proxygroups.component.manage.html',
})
export class ManageDialog {

  constructor(public dialogRef: MatDialogRef<ManageDialog>, @Inject(MAT_DIALOG_DATA) public data: string,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
  }

  onCloseClick(): void {
    this.dialogRef.close();
  }

  onStartClick(name: string, count: string) {
    this._cloud.postStartProxyGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name, parseInt(count)).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }

  onShutdownClick(name: string): void {
    this._cloud.postShutdownProxyGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), name).subscribe(() => {
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }
}

@Component({
  selector: 'edit-dialog',
  templateUrl: 'proxygroups.component.edit.html',
  styleUrls: ['./proxygroups.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EditDialog {
  proxygroup: ProxyGroup;
  WrapperControl = new FormControl();
  filteredWrappers: Observable<string[]>;
  wrappers: string[] = [];
  private version;


  constructor(public dialogRef: MatDialogRef<EditDialog>, @Inject(MAT_DIALOG_DATA) public data: ProxyGroup,
              public _cloud: CloudConnectorService, public _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
    this.proxygroup = data;
    this._cloud.getWrappers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.wrappers = t['response'];
    });
    this.filteredWrappers = this.WrapperControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this._cloud.getCloudNetVersion(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(value=>{
      this.version = value['response'];
    })

  }

  onCloseClick(): void {
    this.dialogRef.close(true);
  }

  onSaveClick(): void {
    this._cloud.postSaveProxyGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.proxygroup).subscribe(() => {
      this._notify.UpdateSuccesfully();
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }

  removeWhitelistPlayer(player: string) {
    const index: number = this.proxygroup.proxyConfig.whitelist.indexOf(player);
    if (index !== -1) {
      this.proxygroup.proxyConfig.whitelist.splice(index, 1);
    }

  }

  addWhitelistPlayer(player: string) {
    const index: number = this.proxygroup.proxyConfig.whitelist.indexOf(player);
    if (index === -1) {
      this.proxygroup.proxyConfig.whitelist.push(player);
    }
  }

  saveProtocol(protocol: string) {
    this.proxygroup.proxyConfig.maintenaceProtocol = protocol;
  }

  saveMaintenanceMotdFirstLine(value: string) {
    this.proxygroup.proxyConfig.maintenanceMotdLayout.firstLine = value;
  }

  saveMaintenanceMotdSecondLine(value: string) {
    this.proxygroup.proxyConfig.maintenanceMotdLayout.secondLine = value;
  }

  saveMotdF(value: string, int: number) {
    this.proxygroup.proxyConfig.motdsLayouts[int].firstLine = value;
  }

  saveMotdS(value: string, int: number) {
    this.proxygroup.proxyConfig.motdsLayouts[int].secondLine = value;
  }

  addModt(int: number, firstline: string, secondline: string) {

    let motd: Motd;
    motd = {firstLine: firstline, secondLine: secondline};
    this.proxygroup.proxyConfig.motdsLayouts.push(motd);
  }

  removeMotd(int: number) {
    if (this.proxygroup.proxyConfig.motdsLayouts.length === 1) {
      return;
    }
    this.proxygroup.proxyConfig.motdsLayouts.splice(int, 1);
  }

  saveTabListHeader(header: string) {
    this.proxygroup.proxyConfig.tabList.header = header;
  }

  saveTabListFooter(footer: string) {
    this.proxygroup.proxyConfig.tabList.footer = footer;
  }

  saveName(name: string) {
    this.proxygroup.name = name;
  }

  saveMemory(memory: string) {
    this.proxygroup.memory = parseInt(memory);
  }

  saveProxyPort(port: string) {
    this.proxygroup.startPort = parseInt(port);
  }

  saveStartup(startup: string) {
    this.proxygroup.startup = parseInt(startup);
  }

  saveMaxPlayers(maxplayers: string) {
    this.proxygroup.proxyConfig.maxPlayers = parseInt(maxplayers);
  }

  saveFallback(fallback: string) {
    this.proxygroup.proxyConfig.dynamicFallback.defaultFallback = fallback;
  }

  saveProxyVersion(version: ProxyVersion) {
    this.proxygroup.proxyVersion = version;
  }

  saveGroupMode(groupmode: ProxyGroupMode) {
    this.proxygroup.proxyGroupMode = groupmode;
  }

  addWrapper(name: string) {
    this.proxygroup.wrapper.push(name);
  }

  removeWrapper(name: string) {
    if (this.proxygroup.wrapper.length === 1) {
      return;
    }
    const index: number = this.proxygroup.wrapper.indexOf(name);
    if (index !== -1) {
      this.proxygroup.wrapper.splice(index, 1);
    }
  }

  addFallback(groupname: string, permission: string) {
    const server: ServerFallback = {group: groupname, permission: permission};
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks.push(server);
  }

  saveFallBackName(value: string, int: number) {
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks[int].group = value;
  }

  saveFallBackPermission(value: string, int: number) {
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks[int].permission = value;
  }

  removeFallback(int: number) {
    if (this.proxygroup.proxyConfig.dynamicFallback.fallbacks.length === 1) {
      return;
    }
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks.splice(int, 1);
  }

  saveTemplateName(value: string) {
    this.proxygroup.template.name = value;
  }

  saveTemplateUrl(value: string) {
    this.proxygroup.template.url = value;
  }

  saveTemplateBackend(value: string) {
    this.proxygroup.template.backend = TemplateResource[value];
  }

  addPlugin(name: string, url: string, type: string) {
    const plugin: ServerInstallablePlugin = {name: name, pluginResourceType: PluginResourceType[type], url: url};
    this.proxygroup.template.intallablePlugins.push(plugin);
  }

  savePluginName(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].name = value;
  }

  savePluginUrl(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].url = value;
  }

  savePluginResourceType(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].pluginResourceType = PluginResourceType[value];
  }


  saveDynamicSlotSize(int: string) {
    this.proxygroup.proxyConfig.autoSlot.dynamicSlotSize = parseInt(int);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.wrappers.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }

  replaceColor(value: string) {


    return value
      .replace(/&4/g, '<span style="color: #AA0000; text-shadow: 1px 1px 0 #2A0000;" >')
      .replace(/&c/g, '<span style="color: #FF5555; text-shadow: 1px 1px 0 #3F1515;" >')
      .replace(/&6/g, '<span style="color: #FFAA00; text-shadow: 1px 1px 0 #2A2A00;" >')
      .replace(/&e/g, '<span style="color: #FFFF55; text-shadow: 1px 1px 0 #3F3F15;" >')
      .replace(/&2/g, '<span style="color: #00AA00; text-shadow: 1px 1px 0 #002A00;" >')
      .replace(/&a/g, '<span style="color: #55FF55; text-shadow: 1px 1px 0 #153F15;" >')
      .replace(/&b/g, '<span style="color: #55FFFF; text-shadow: 1px 1px 0 #153F3F;" >')
      .replace(/&3/g, '<span style="color: #00AAAA; text-shadow: 1px 1px 0 #002A2A;" >')
      .replace(/&1/g, '<span style="color: #0000AA; text-shadow: 1px 1px 0 #00002A;" >')
      .replace(/&9/g, '<span style="color: #5555FF; text-shadow: 1px 1px 0 #15153F;" >')
      .replace(/&d/g, '<span style="color: #FF55FF; text-shadow: 1px 1px 0 #3F153F;" >')
      .replace(/&5/g, '<span style="color: #AA00AA; text-shadow: 1px 1px 0 #2A002A;" >')
      .replace(/&7/g, '<span style="color: #AAAAAA; text-shadow: 1px 1px 0 #2A2A2A;" >')
      .replace(/&8/g, '<span style="color: #555555; text-shadow: 1px 1px 0 #151515;" >')
      .replace(/&0/g, '<span style="color: #000000; text-shadow: 1px 1px 0 #000000;" >')
      .replace(/&f/g, '<span style="color: #FFFFFF; text-shadow: 1px 1px 0 #3F3F3F;" >')

      .replace(/&l/g, '<span style="font-weight: bold">')
      .replace(/&n/g, '<span style="text-decoration: underline">')
      .replace(/&o/g, '<span style="font-style: italic;">')
      .replace(/&m/g, '<span style="text-decoration: line-through">')
      .replace(/&r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/§4/g, '<span style="color: #fe0000">')
      .replace(/§c/g, '<span style="color: #ff4c52">')
      .replace(/§6/g, '<span style="color: #ff8e42">')
      .replace(/§e/g, '<span style="color: #ffd800">')
      .replace(/§2/g, '<span style="color: #31a310">')
      .replace(/§a/g, '<span style="color: #4dd800">')
      .replace(/§b/g, '<span style="color: #01ffff">')
      .replace(/§3/g, '<span style="color: #0094fe">')
      .replace(/§1/g, '<span style="color: #0026ff">')
      .replace(/§9/g, '<span style="color: #4368ff">')
      .replace(/§d/g, '<span style="color: #fe00dc">')
      .replace(/§5/g, '<span style="color: #b100fe">')
      .replace(/§7/g, '<span style="color: #a0a0a0">')
      .replace(/§8/g, '<span style="color: #404040">')
      .replace(/§0/g, '<span style="color: #010101">')
      .replace(/§f/g, '<span style="color: #ffffff">')

      .replace(/§l/g, '<span style="font-weight: bold">')
      .replace(/§n/g, '<span style="text-decoration: underline">')
      .replace(/§o/g, '<span style="font-style: italic;">')
      .replace(/§m/g, '<span style="text-decoration: line-through">')
      .replace(/§r/g, '<span style="color: white; text-decoration: none; font-weight: normal; font-style: normal">')
      .replace(/%version%/g, this.version);
  }

}

@Component({
  selector: 'add-dialog',
  templateUrl: 'proxygroups.component.add.html',
})
export class AddDialog {
  WrapperControl = new FormControl();
  filteredWrappers: Observable<string[]>;
  wrappers: string[] = [];
  proxygroup: ProxyGroup;

  constructor(public dialogRef: MatDialogRef<AddDialog>, @Inject(MAT_DIALOG_DATA) public data: ProxyGroup,
              public _cloud: CloudConnectorService, private _notify: NotificationsService,
              private cookie: CookieService,private _es: ElectronService) {
    this.proxygroup = data;
    this._cloud.getWrappers(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t => {
      this.wrappers = t['response'];
    });
    this.filteredWrappers = this.WrapperControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
  }

  onCloseClick(): void {
    this.dialogRef.close(false);
  }

  onSaveClick(): void {
    this._cloud.postSaveProxyGroup(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')), this.proxygroup).subscribe(() => {
      this._notify.AddSuccesfully();
      this.dialogRef.close(true);
    }, error => {
      if ((error.status === 403)) {
        this._notify.AccessDenied();
      }
    });
  }

  removeWhitelistPlayer(player: string) {
    const index: number = this.proxygroup.proxyConfig.whitelist.indexOf(player);
    if (index !== -1) {
      this.proxygroup.proxyConfig.whitelist.splice(index, 1);
    }

  }

  addWhitelistPlayer(player: string) {
    const index: number = this.proxygroup.proxyConfig.whitelist.indexOf(player);
    if (index === -1) {
      this.proxygroup.proxyConfig.whitelist.push(player);
    }
  }

  saveProtocol(protocol: string) {
    this.proxygroup.proxyConfig.maintenaceProtocol = protocol;
  }

  saveMaintenanceMotdFirstLine(value: string) {
    this.proxygroup.proxyConfig.maintenanceMotdLayout.firstLine = value;
  }

  saveMaintenanceMotdSecondLine(value: string) {
    this.proxygroup.proxyConfig.maintenanceMotdLayout.secondLine = value;
  }

  saveMotdF(value: string, int: number) {
    this.proxygroup.proxyConfig.motdsLayouts[int].firstLine = value;
  }

  saveMotdS(value: string, int: number) {
    this.proxygroup.proxyConfig.motdsLayouts[int].secondLine = value;
  }

  addModt(int: number, firstline: string, secondline: string) {

    let motd: Motd;
    motd = {firstLine: firstline, secondLine: secondline};
    this.proxygroup.proxyConfig.motdsLayouts.push(motd);
  }

  removeMotd(int: number) {
    if (this.proxygroup.proxyConfig.motdsLayouts.length === 1) {
      return;
    }
    this.proxygroup.proxyConfig.motdsLayouts.splice(int, 1);
  }

  saveTabListHeader(header: string) {
    this.proxygroup.proxyConfig.tabList.header = header;
  }

  saveTabListFooter(footer: string) {
    this.proxygroup.proxyConfig.tabList.footer = footer;
  }

  saveName(name: string) {
    this.proxygroup.name = name;
  }

  /*
  @later
  savePlayerInfo(value: string[]) {
    this.proxygroup.proxyConfig.playerInfo = value;
  }*/
  saveMemory(memory: string) {
    this.proxygroup.memory = parseInt(memory);
  }

  saveProxyPort(port: string) {
    this.proxygroup.startPort = parseInt(port);
  }

  saveStartup(startup: string) {
    this.proxygroup.startup = parseInt(startup);
  }

  saveMaxPlayers(maxplayers: string) {
    this.proxygroup.proxyConfig.maxPlayers = parseInt(maxplayers);
  }

  saveFallback(fallback: string) {
    this.proxygroup.proxyConfig.dynamicFallback.defaultFallback = fallback;
  }

  saveProxyVersion(version: string) {
    this.proxygroup.proxyVersion = ProxyVersion[version];
  }

  saveGroupMode(groupmode: string) {
    this.proxygroup.proxyGroupMode = ProxyGroupMode[groupmode];
  }

  addWrapper(name: string) {
    this.proxygroup.wrapper.push(name);
  }

  removeWrapper(name: string) {
    if (this.proxygroup.wrapper.length === 1) {
      return;
    }
    const index: number = this.proxygroup.wrapper.indexOf(name);
    if (index !== -1) {
      this.proxygroup.wrapper.splice(index, 1);
    }
  }

  addFallback(groupname: string, permission: string) {
    const server: ServerFallback = {group: groupname, permission: permission};
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks.push(server);
  }

  saveFallBackName(value: string, int: number) {
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks[int].group = value;
  }

  saveFallBackPermission(value: string, int: number) {
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks[int].permission = value;
  }

  removeFallback(int: number) {
    if (this.proxygroup.proxyConfig.dynamicFallback.fallbacks.length === 1) {
      return;
    }
    this.proxygroup.proxyConfig.dynamicFallback.fallbacks.splice(int, 1);
  }

  saveTemplateName(value: string) {
    this.proxygroup.template.name = value;
  }

  saveTemplateUrl(value: string) {
    this.proxygroup.template.url = value;
  }

  saveTemplateBackend(value: string) {
    this.proxygroup.template.backend = TemplateResource[value];
  }

  addPlugin(name: string, url: string, type: string) {
    const plugin: ServerInstallablePlugin = {name: name, pluginResourceType: PluginResourceType[type], url: url};
    this.proxygroup.template.intallablePlugins.push(plugin);
  }

  savePluginName(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].name = value;
  }

  savePluginUrl(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].url = value;
  }

  savePluginResourceType(value: string, int: number) {
    this.proxygroup.template.intallablePlugins[int].pluginResourceType = PluginResourceType[value];
  }

  saveDynamicSlotSize(int: string) {
    this.proxygroup.proxyConfig.autoSlot.dynamicSlotSize = parseInt(int);
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.wrappers.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
  }
}

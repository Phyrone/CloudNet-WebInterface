/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

/**
 * Allgemine Imports um den Guard zu benutzen
 */
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {Observable} from 'rxjs';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';

/**
 * Defeniert den Guard mit name und Tpye
 * @CanActivate - Implementiert die Methode canActivate
 */
@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(private router: Router,private cookie:CookieService,private _es: ElectronService) {
  }

  /**
   * Wird ausgelöst sobald der Benutzer ein RouteLink anklickt
   * @param {ActivatedRouteSnapshot} next
   * @param {RouterStateSnapshot} state
   * @returns {Observable<boolean> | Promise<boolean> | boolean}
   */
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')) {
      //Gibt True(Wahr) zurück wenn der Benutzer eingeloggt ist
      return true;
    }

    // Gibt False(Nicht Wahr) zurück wenn der Benutze nicht eingeloggt ist und Navigiert ihn zum Login
    this.router.navigate(['login'], {queryParams: {returnUrl: state.url}}).catch();
    return false;
  }
}

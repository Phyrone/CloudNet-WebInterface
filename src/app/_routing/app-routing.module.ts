/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {MyDashboardComponent} from '../pages/dashboard/my-dashboard.component';
import {LoginComponent} from '../pages/login/login.component';
import {AuthGuard} from '../_guards/auth-guard.guard';
import {ProxygroupsComponent} from '../pages/proxygroups/proxygroups.component';
import {UsermangmentComponent} from '../pages/usermangment/usermangment.component';
import {ServergroupsComponent} from '../pages/servergroups/servergroups.component';
import {ProxyListComponent} from '../pages/proxylist/proxy-list.component';
import {ServerListComponent} from '../pages/serverlist/server-list.component';
import {CpermsComponent} from '../pages/cperms/cperms.component';
import {PermissionGroupComponent} from '../pages/cperms/permission-group/permission-group.component';
import {PermissionNavigationComponent} from '../pages/cperms/permission-navigation/permission-navigation.component';
import {PermissionUserComponent} from '../pages/cperms/permission-user/permission-user.component';
import {AddGroupComponent} from '../pages/cperms/add-group/add-group.component';
import {WrapperListComponent} from '../pages/wrapper-list/wrapper-list.component';
import {SignComponent} from '../pages/sign/sign.component';

const routes: Routes = [
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'dashboard', component: MyDashboardComponent, canActivate: [AuthGuard]},
  {path: 'proxygroups', component: ProxygroupsComponent, canActivate: [AuthGuard]},
  {path: 'servergroups', component: ServergroupsComponent, canActivate: [AuthGuard]},
  {path: 'proxylist', component: ProxyListComponent, canActivate: [AuthGuard]},
  {path: 'serverlist', component: ServerListComponent, canActivate: [AuthGuard]},
  {path: 'wrappers', component: WrapperListComponent, canActivate: [AuthGuard]},
  {path: 'sign', component: SignComponent, canActivate: [AuthGuard]},
  {path: 'users', component: UsermangmentComponent, canActivate: [AuthGuard]},
  {
    path: 'cperms', component: CpermsComponent, canActivate: [AuthGuard], children:
      [
        {path: 'editgroup/:group', component: PermissionGroupComponent, canActivate: [AuthGuard]},
        {path: 'edituser/:user', component: PermissionUserComponent, canActivate: [AuthGuard]},
        {path: 'addgroup', component: AddGroupComponent, canActivate: [AuthGuard]},
        {path: '', component: PermissionNavigationComponent, canActivate: [AuthGuard]},

      ]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes,{useHash:true}),
    CommonModule,
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})

export class AppRoutingModule {
}

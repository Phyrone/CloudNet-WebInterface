/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Pipe, PipeTransform } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';

@Pipe({
  name: 'millisInTime'
})
export class MillisInTimePipe implements PipeTransform {

  constructor(private _:TranslateService){

  }

  transform(value: number): any {
    let milliseconds = Math.round((value%1000)/100)
      , seconds = Math.round((value/1000)%60)
      , minutes = Math.round((value/(1000*60))%60)
      , hours = Math.round((value/(1000*60*60))%24)
      , days = Math.round((value/(1000*60*60*24)))
      , weeks = Math.round((value/(1000*60*60*24*7)))
      , months = Math.round((value/(1000*60*60*24*7*4)))
      , years = Math.round((value/(1000*60*60*24*7*4*12)));

    let hoursString = (hours < 10) ? "0" + hours : hours;
    let minutesString = (minutes < 10) ? "0" + minutes : minutes;
    let secondsString = (seconds < 10) ? "0" + seconds : seconds;
    let daysString = (days < 10) ? "0" + days : days;
    let weeksString = (weeks < 10) ?  "0" + weeks : weeks;
    let monthsString = (months < 10) ?  "0" + months : months;
    let yearsString = (years < 10) ?  "0" + years : years;
    let yearTrans = '';
    let monthsTrans = '';
    let weeksTrans = '';
    let daysTrans = '';
    let hoursTrans = '';
    let minutesTrans = '';
    let secondsTrans = '';
    let milliSecondsTrans = '';
    this._.get("page.dashboard.text.cloudstats.dateformat.year").subscribe(t=>yearTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.month").subscribe(t=>monthsTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.week").subscribe(t=>weeksTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.day").subscribe(t=>daysTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.hour").subscribe(t=>hoursTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.minute").subscribe(t=>minutesTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.second").subscribe(t=>secondsTrans = t);
    this._.get("page.dashboard.text.cloudstats.dateformat.milli.second").subscribe(t=>milliSecondsTrans = t);
    return yearsString    + " "+yearTrans    + ","
          + monthsString  + " "+monthsTrans + ","
          + weeksString   + " "+weeksTrans   + ","
          + daysString    + " "+daysTrans    + ","
          + hoursString   + " "+hoursTrans   + ","
          + minutesString + " "+minutesTrans + ","
          + secondsString + " "+secondsTrans + ","
          + milliseconds  + " "+milliSecondsTrans;
  }

}

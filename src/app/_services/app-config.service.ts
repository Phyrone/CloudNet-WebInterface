/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {IAppConfig} from '../_models';
import {UUID} from 'angular2-uuid';
import {ElectronService} from 'ngx-electron';
import {VersionType} from '../_models/version-type.enum';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class AppConfigService {

  static settings: IAppConfig = {
    GoogleRecaptcha: {
      enabled: false,
      SiteKey: ''
    },
    updateChannel: VersionType.RELEASE,
    Servers: [
      {CloudName: 'Unknwon Settings - 127.0.0.1',
      CloudURL: 'http://127.0.0.1:1420/'
      }
    ],
    settings: {
      branding: 'SETTINGS NOT FOUND',
      timeout: -1,
      interval: {
        console: 1000,
        dashboard: 2500
      }
    },
    style: {
      default: 'mad-theme'
    },
    analytics: {
      enabled: true,
      ID: ''
    }
  };

  constructor(private http: HttpClient,private _es: ElectronService) {
  }

  load() {
    const jsonFile = this._es.isElectronApp ? 'assets/config/config.json': 'assets/config/config.json?key=' + UUID.UUID();
    return this.http.get(jsonFile).subscribe((resp: IAppConfig) => {
      AppConfigService.settings = <IAppConfig>resp;
    });
  }
}

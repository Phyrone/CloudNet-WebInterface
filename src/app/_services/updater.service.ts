/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {WiVersion} from '../_models/wi-version';
import {NotificationsService} from './notifications.service';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class UpdaterService implements OnInit {
  public settings: WiVersion;

  constructor(private http: HttpClient, public _notify: NotificationsService) {
  }


  load() {
    const jsonFile = `assets/config/version.json`;
    return this.http.get(jsonFile).subscribe((resp: WiVersion) => {
      this.settings = resp;
    });
  }

  ngOnInit(): void {
    this.load();
  }

}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {User} from '../_models/user';
import {Observable} from 'rxjs';
import {ProxyGroup} from '../_models/proxyGroup';
import {CloudNetWork} from '../_models';
import {ServerGroup} from '../_models/server-group';
import {PermissionGroup} from '../_models/permission-group';
import {OfflinePlayer} from '../_models/offline-player';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';
import {SignLayoutConfig} from '../_models/sign-layout-config';


@Injectable(
  {
    providedIn: 'root',
  }
)
export class CloudConnectorService {


  constructor(private http: HttpClient, private cookie: CookieService,private _es: ElectronService) {
  }

  getCloudStats(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'cloudstats',
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/utils', {headers: headers});
  }
  getServerConsole(user: User, value: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'screen',
      '-Xvalue': value
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', {headers: headers});
  }

  getProxyConsole(user: User, value: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'screen',
      '-Xvalue': value
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', {headers: headers});
  }
  getBadge(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xmessage': 'badges',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/utils', {headers: headers});
  }
  getCloudNetVersion(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xmessage': 'cloudversion',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/utils', {headers: headers});

  }
  getWrapperInfos(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'warpperinfos'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/wrapper', {headers: headers});
  }

  getMasterLog(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'corelog'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/master', {headers: headers});
  }

  getWrappers(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'wrappers'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/wrapper', {headers: headers});
  }

  getPlayerCount(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'players'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/dashboard', {headers: headers});
  }

  getProxyCount(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'proxys'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/dashboard', {headers: headers});
  }

  getServerCount(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'servers'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/dashboard', {headers: headers});
  }

  getGroupCount(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'groups'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/dashboard', {headers: headers});
  }

  getUserItems(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'users'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/userapi', {headers: headers});
  }

  getMinecraftServerGroups(user: User): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'groups'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', {headers: headers});
  }

  getProxyGroups(user: User): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'groups'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', {headers: headers});
  }

  getProxyServers(user: User, value: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'proxys',
      '-Xvalue': value
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', {headers: headers});
  }

  isCloudPermission(user: User): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'check'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', {headers: headers});
  }
  getMinecraftServers(user: User, value: string): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'servers',
      '-Xvalue': value
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', {headers: headers});
  }
  getPermissionUser(user: User, name: string): Observable<any> {
    const headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'user',
        '-xValue': name

      });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', {headers: headers});
  }
  getPermissionGroup(user: User, name: string): Observable<any> {
    let headers;
    if (name == null) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group'
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group',
        '-xValue': name

      });
    }
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', {headers: headers});
  }

  getServer(user: User, name: string): Observable<any> {
    let headers;
    if (name == null) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group'
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group',
        '-xValue': name

      });
    }
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', {headers: headers});
  }

  getProxy(user: User, name: string): Observable<any> {
    let headers;
    if (name == null) {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group'
      });
    } else {
      headers = new HttpHeaders({
        'Content-Type': 'application/json; charset=utf-8',
        '-Xcloudnet-user': user.name,
        '-Xcloudnet-token': user.token,
        '-Xmessage': 'group',
        '-xValue': name

      });
    }

    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', {headers: headers});
  }

  postDisableScreenMinecraftServer(user: User, name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'stopscreen',
      '-Xvalue': name,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', '', {headers: headers});
  }

  postDisableScreenProxy(user: User, name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'stopscreen',
      '-Xvalue': name,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', '', {headers: headers});
  }

  postDeleteProxyGroup(user: User, name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'delete',
      '-Xvalue': name,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', '', {headers: headers});
  }

  postShutdownProxyGroup(user: User, name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'stop',
      '-Xvalue': name,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', '', {headers: headers});
  }

  postSaveProxyGroup(user: User, value: ProxyGroup) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'save'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', JSON.stringify(value), {headers: headers});
  }

  postStartProxyGroup(user: User, name: string, count: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'start',
      '-Xvalue': name,
      '-Xcount': '' + count,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', '', {headers: headers});
  }

  postUpdateUser(user: User, username: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'save'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/userapi', JSON.stringify(username), {headers: headers});
  }

  postAddUser(user: User, username: string, password: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'add'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/userapi', JSON.stringify({
      username: username,
      password: btoa(password)
    }), {headers: headers});
  }

  postResetUserPassword(user: User, username: string, password: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'resetpassword'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/userapi', JSON.stringify({
      username: username,
      password: btoa(password)
    }), {headers: headers});
  }

  postDeleteUser(user: User, username: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'delete',
      '-Xvalue': username
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/userapi', null, {headers: headers});
  }

  postStartServerGroup(user: User, name: string, count: number) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'start',
      '-Xvalue': name,
      '-Xcount': '' + count,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', '', {headers: headers});
  }

  postShutdownServerGroup(user: User, name: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'stop',
      '-Xvalue': name,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', '', {headers: headers});
  }

  postMasterCommand(user: User, value: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'command',
      '-Xvalue': value,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/master', '', {headers: headers});
  }

  postDeleteServerGroup(user: User, value: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'delete',
      '-Xvalue': value,
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', '', {headers: headers});
  }

  postSaveServerGroup(user: User, group: ServerGroup) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'save'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', JSON.stringify(group), {headers: headers});
  }
  postSendPlayer(user: User,server: string,player: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'send',
      '-Xvalue' : player,
      '-Xcount' : server
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/player', '', {headers: headers});
  }
  postMasterStop(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'stop'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/master', '', {headers: headers});
  }


  postMasterReloadAll(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'reloadall'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/master', '', {headers: headers});
  }

  postMasterClearCache(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'clearcache'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/master', '', {headers: headers});
  }

  postServerCommand(user: User, command: string, serverId: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'command',
      '-Xvalue': serverId,
      '-Xcount': command
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/servergroup', '', {headers: headers});
  }

  postProxyCommand(user: User, command: string, serverId: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'command',
      '-Xvalue': serverId,
      '-Xcount': command
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/proxygroup', '', {headers: headers});
  }
  postPermissionGroupDelete(user: User, group: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'deletegroup',
      '-xValue': group
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', '', {headers: headers});
  }
  updatePermissionUser(user: User, group: OfflinePlayer) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'user'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', JSON.stringify(group), {headers: headers});
  }
  updatePermissionGroup(user: User, group: PermissionGroup) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'group'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', JSON.stringify(group), {headers: headers});
  }
  getRandomServer(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'random'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/sign', {headers: headers});
  }
  getSignConfig(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'config'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/sign', {headers: headers});
  }
  postSignConfig(user: User,config: SignLayoutConfig) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'save'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/sign', JSON.stringify(config),{headers: headers});
  }
  getGroups(user: User) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'groups'
    });
    return this.http.post((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/cperms', '', {headers: headers});
  }

  isCloudSignSystem(user: User): Observable<any> {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': user.name,
      '-Xcloudnet-token': user.token,
      '-Xmessage': 'check'
    });
    return this.http.get((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL +
      '/cloudnet/api/v2/sign', {headers: headers});
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import 'rxjs/add/operator/map';
import {CloudNetWork} from '../_models';
import {BehaviorSubject} from 'rxjs';
import {NotificationsService} from './notifications.service';
import {CookieService} from 'ngx-cookie-service';
import {ElectronService} from 'ngx-electron';

@Injectable(
  {
    providedIn: 'root',
  }
)
export class AuthenticationService {
  public static isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);

  constructor(private http: HttpClient, public _notify: NotificationsService,private cookie: CookieService,private _es: ElectronService) {
    AuthenticationService.isUserLoggedIn.next(this._es.isElectronApp?localStorage.getItem("currentCloud") !== null: this.cookie.check('currentCloud'));
  }

  login(username: string, password: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json; charset=utf-8',
      '-Xcloudnet-user': username,
      '-Xcloudnet-password': password
    });
    return this.http.post<any>((JSON.parse(this._es.isElectronApp?localStorage.getItem("currentCloud") : this.cookie.get('currentCloud')) as CloudNetWork).CloudURL + '/cloudnet/api/v2/auth', null, {headers: headers});
  }

  logout() {
    // remove user from local storage to log user out
    AuthenticationService.isUserLoggedIn.next(false);
    if(this._es.isElectronApp){
      localStorage.removeItem("currentUser");
      localStorage.removeItem("currentCloud");

    }else{
      this.cookie.delete('currentUser');
      this.cookie.delete('currentCloud');
    }
  }
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'memory'
})
export class MemoryPipe implements PipeTransform {

  transform(bytes: any): any {
    if(bytes < 1024) return bytes + " MB";
    else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " GB";
    else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " TB";
    else return(bytes / 1073741824).toFixed(3) + " PT";
  }

}

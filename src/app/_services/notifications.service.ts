/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Injectable} from '@angular/core';
import {SnotifyService} from 'ng-snotify';
import {TranslateService} from '@ngx-translate/core';

@Injectable({
  providedIn: 'root',
})
export class NotificationsService {

  constructor(private snotifyService: SnotifyService, public _: TranslateService) {
    snotifyService.setDefaults({
      global: {
        newOnTop: true,
        maxAtPosition: 4,
        maxOnScreen: 8
      }
    });
  }

  public AccessDenied() {
    let message = '';
    let title = '';
    this._.get('notification.permissions.error.text').subscribe(t => {
      message = t;
    });
    this._.get('notification.permissions.error.title').subscribe(t => {
      title = t;
    });

    this.snotifyService.error(message, title, {timeout: 5000});

  }

  public LoginError() {
    let message = '';
    let title = '';
    this._.get('notification.login.error.text').subscribe(t => {
      message = t;
    });
    this._.get('notification.login.error.title').subscribe(t => {
      title = t;
    });
    this.snotifyService.error(message, title, {timeout: 5000});
  }
  public LastItemError() {
    let message = '';
    let title = '';
    this._.get('notification.lastitem.error.text').subscribe(t => {
      message = t;
    });
    this._.get('notification.lastitem.error.title').subscribe(t => {
      title = t;
    });
    this.snotifyService.error(message, title, {timeout: 7000});
  }

  public UpdateSuccesfully() {
    let message = '';
    let title = '';
    this._.get('notification.update.text').subscribe(t => {
      message = t;
    });
    this._.get('notification.update.title').subscribe(t => {
      title = t;
    });
    this.snotifyService.success(message, title, {timeout: 7000});
  }

  public AddSuccesfully() {
    let message = '';
    let title = '';
    this._.get('notification.add.text').subscribe(t => {
      message = t;
    });
    this._.get('notification.add.title').subscribe(t => {
      title = t;
    });
    this.snotifyService.success(message, title, {timeout: 7000});
  }


  public PlayerNotFoundError() {
    this.snotifyService.error('Player not Found!', 'CloudNet Permission System', {timeout: 5000});
  }

  DeleteSuccessfully() {
    this.snotifyService.success('Succefully Deleted!', 'Delete', {timeout: 7000});
  }
}

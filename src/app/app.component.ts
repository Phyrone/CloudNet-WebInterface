/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Component, HostBinding, OnInit} from '@angular/core';
import {User} from './_models/user';
import {AuthenticationService} from './_services/authentication.service';
import {CloudConnectorService} from './_services/cloud-connector.service';
import 'rxjs-compat/add/operator/take';
import {OverlayContainer} from '@angular/cdk/overlay';
import {MatDialog} from '@angular/material';
import {TranslateService} from '@ngx-translate/core';
import {NotificationsService} from './_services/notifications.service';
import {MasterInterfaceComponent} from './popup/master-interface/master-interface.component';
import {PlayerConsoleComponent} from './popup/player-console/player-console.component';
import {CookieService} from 'ngx-cookie-service';
import {AppConfigService} from './_services/app-config.service';
import {ClientSettingsComponent} from './pages/client-settings/client-settings.component';
import {DisclaimerComponent} from './pages/disclaimer/disclaimer.component';
import {ChangelogComponent} from './pages/changelog/changelog.component';
import {AboutComponent} from './pages/about/about.component';
import {ElectronService} from 'ngx-electron';
import {Observable} from 'rxjs';
import {UpdaterService} from './_services/updater.service';
import {MatomoInjector, MatomoTracker} from 'ngx-matomo';
import {NavigationEnd, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{

  currentUser: User = {name: 'Unknown',password: '',permissions: [],token: '',uuid: ''};
  cperms = true;
  BRANDING = '';
  @HostBinding('class') componentCssClass;
  language: string = 'en';

  proxy_groups:number = 0;
  server_groups:number = 0;
  proxies:number = 0;
  servers:number = 0;
  wrappers:number = 0;
  sign: boolean = false;

  constructor(private authService: AuthenticationService, public overlayContainer: OverlayContainer, public dialog: MatDialog,
              private _cloud: CloudConnectorService, private _notify: NotificationsService,
              private translate: TranslateService,
              private cookie: CookieService,private _es: ElectronService,
              private _update: UpdaterService,
              private matomoInjector: MatomoInjector,
              private matomoTracker: MatomoTracker,
              private appConfig: AppConfigService,
              private route: Router) {
    appConfig.load();
    if(this._es.isElectronApp){
      if(localStorage.getItem('currentLanguage') !== null){
        this.useLanguage(localStorage.getItem("currentLanguage"));
        this.translate.use(localStorage.getItem("currentLanguage"));
      }else{
        this.translate.setDefaultLang('en');
        this.translate.use(this.translate.getBrowserLang());
        this.language = this.translate.getBrowserLang();
        localStorage.setItem("currentLanguage",this.translate.getBrowserLang());
      }
    }else{
      if(this.cookie.check('currentLanguage')){
        this.useLanguage(this.cookie.get("currentLanguage"));
        this.translate.use(this.cookie.get("currentLanguage"));
      }else{
        this.translate.setDefaultLang('en');
        this.translate.use(this.translate.getBrowserLang());
        this.language = this.translate.getBrowserLang();
        this.cookie.set("currentLanguage",this.translate.getBrowserLang(),0,'/');
      }
    }
    if(AppConfigService.settings.analytics.enabled === true){
      this.matomoInjector.init('https://analytics.madfix.me/', 1);
      this.matomoTracker.setUserId(AppConfigService.settings.analytics.ID.toString());
      this.matomoTracker.enableLinkTracking(true);
      this.matomoTracker.trackPageView();
      setTimeout(()=>{


        if(AppConfigService.settings.analytics.enabled === true)this._cloud.getCloudNetVersion((JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')) as User))
          .subscribe(t=>this.matomoTracker.trackEvent('information','cloudnet',t.toString()));
        if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('information','webinterface',this._update.settings.version + '-' + this._update.settings.type.toString());
      },200);
    }
    route.events.subscribe(event=>{
      if(event instanceof NavigationEnd){
        if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('site',event.url);
        if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackPageView(event.urlAfterRedirects);
      }
    });
    if(localStorage.getItem('dashboard.interval') == null){
      localStorage.setItem( 'dashboard.interval', AppConfigService.settings.settings.interval.dashboard.toString());
    }
    if(localStorage.getItem('dashboard.console') == null){
      localStorage.setItem( 'dashboard.console', AppConfigService.settings.settings.interval.console.toString());
    }

  }

  ngOnInit() {

    if(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')){
      this.currentUser = JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'));
    }
    if(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')){
      this.currentUser = JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'));
    }
    if(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')){
      this._cloud.isCloudPermission(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((t) => {
        this.cperms = t['success'] as boolean;
      }, error1 => {
        if (error1.code === 400) {
          this.cperms = false;
        }
      });
      this._cloud.isCloudSignSystem(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe((t) => {
        this.sign = t['response'] as boolean;
      }, error1 => {
        if (error1.code === 400) {
          this.sign = false;
        }
      });
    }
    setTimeout(() => {
      this._update.load();

      if(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser')){
        this.currentUser = JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'));
      }
      Observable.interval(parseInt(localStorage.getItem('console.interval'))).subscribe(()=>{
        this._cloud.getBadge(JSON.parse(this._es.isElectronApp ?localStorage.getItem("currentUser"): this.cookie.get('currentUser'))).subscribe(t=>{
          let badges = t['response'];
          this.proxy_groups = badges['proxy_groups'];
          this.server_groups = badges['server_groups'];
          this.proxies = badges['proxies'];
          this.servers = badges['servers'];
          this.wrappers = badges['wrappers'];
        });
      });
    }, 1000);
    setTimeout(() => {
      this.BRANDING = AppConfigService.settings.settings.branding;
      if (localStorage.getItem('currentTheme') != null) {
        this.onSetTheme((JSON.parse(localStorage.getItem('currentTheme')) as any).theme);
      } else {
        this.onSetTheme(AppConfigService.settings.style.default);
      }

    }, 200);

  }
  useLanguage(language: string) {
    this.translate.use(language);
  }

  onSetTheme(theme) {
    this.overlayContainer.getContainerElement().classList.add(theme);
    this.componentCssClass = theme;
    localStorage.setItem('currentTheme', JSON.stringify({'theme': theme}));
  }
  OpenMasterConsole() {
    this.dialog.open(MasterInterfaceComponent,{
      hasBackdrop: false
    });
  }
  OpenPlayerConsole() {
    this.dialog.open(PlayerConsoleComponent,{
      hasBackdrop: false
    });
  }
  isLoggedIn(){
    return AuthenticationService.isUserLoggedIn.getValue();
  }

  OpenSettings() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('navigation', 'open_settings');
    this.dialog.open(ClientSettingsComponent,{
      hasBackdrop: false
    });
  }

  OpenDisclaimer() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('navigation', 'open_disclaimer');
    this.dialog.open(DisclaimerComponent,{
      hasBackdrop: false
    });
  }

  OpenChangelog() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('navigation', 'open_chnagelog');
    this.dialog.open(ChangelogComponent,{
      hasBackdrop: false
    });
  }

  OpenInformation() {
    if(AppConfigService.settings.analytics.enabled === true) this.matomoTracker.trackEvent('navigation', 'open_information');
    this.dialog.open(AboutComponent,{
      hasBackdrop: false
    });
  }
}

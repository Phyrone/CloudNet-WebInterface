/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */
/**
 * Einfache Klasse um ein Netzwerk(CloudNet) zu Defenieren
 */
export interface CloudNetWork {
  CloudURL: string;
  CloudName: string;
}

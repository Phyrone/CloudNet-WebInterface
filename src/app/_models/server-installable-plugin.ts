/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {PluginResourceType} from './plugin-resource-type.enum';

export interface ServerInstallablePlugin {
  name: string;
  pluginResourceType: PluginResourceType;
  url: string;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServerGroupType} from './server-group-type.enum';
import {ServerGroupMode} from './server-group-mode.enum';
import {Template} from './template';
import {AdvancedServerConfig} from './advanced-server-config.enum';
import {PriorityService} from './priority-service';

export interface ServerGroup {
  name: string;
  wrapper: Array<string>;
  kickedForceFallback: boolean;
  serverType: ServerGroupType;
  groupMode: ServerGroupMode;
  globalTemplate: Template;
  templates: Array<Template>;
  memory: number;
  dynamicMemory: number;
  joinPower: number;
  maintenance: boolean;
  minOnlineServers: number;
  maxOnlineServers: number;
  advancedServerConfig: AdvancedServerConfig;
  percentForNewServerAutomatically: number;
  priorityService: PriorityService;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */
/**
 * Gibt an ob die Slots Dynamisch sin sollen
 * Und wieviel Slots immer mehr angegebn sein Sollen
 */
export interface AutoSlot {
  dynamicSlotSize: number;
  enabled: boolean;
}

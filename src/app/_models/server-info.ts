/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {ServerConfig} from './server-config';
import {Template} from './template';
import {ServerState} from './server-state.enum';

export interface ServerInfo {
  serviceId: ServiceId;
  host: string;
  port: number;
  online: boolean;
  players: string[];
  memory: number;
  motd: string;
  onlineCount: number;
  maxPlayers: number;
  serverState: ServerState;
  serverConfig: ServerConfig;
  template: Template;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

/**
 * Gibt den ServerFallback an mit einer Permission
 */
export interface ServerFallback {
  group: string;
  permission: string;
}

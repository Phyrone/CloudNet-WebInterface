/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {WrapperInfo} from './wrapper-info';
import {WrapperMeta} from './wrapper-meta';
import {CloudServer} from './cloud-server';
import {ProxyServer} from './proxy-server';
import {MinecraftServer} from './minecraft-server';

export interface Wrapper {
  wrapperInfo: WrapperInfo;
  networkInfo: WrapperMeta;
  cpuUsage: number;
  usedMemory: number;
  usedMemoryAndWaiting: number;
  serverId: string;
  cloudServers: CloudServer[];
  servers: MinecraftServer[];
  proxys: ProxyServer[];
  queue: number;
}

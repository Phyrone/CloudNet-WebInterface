/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {VersionType} from './version-type.enum';

export interface Update {
  Type: VersionType;
  version: string;
  fixes: string[];
  adds: string[];
  removes: string[];
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export interface WrapperInfo {
  serverId: string;
  hostName: string;
  ready: boolean;
  availableProcessors: number;
  startPort: number;
  process_queue_size: number;
  memory: number
}

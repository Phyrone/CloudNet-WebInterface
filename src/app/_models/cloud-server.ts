
/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {CloudServerMeta} from './cloud-server-meta';
import {ServerInfo} from './server-info';
import {ServerGroupType} from './server-group-type.enum';

export interface CloudServer {
  serviceId: ServiceId;
  cloudServerMeta: CloudServerMeta;
  serverGroupType: ServerGroupType;
  serverInfo: ServerInfo;
}

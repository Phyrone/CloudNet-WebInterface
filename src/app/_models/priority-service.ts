/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {PriorityConfig} from './priority-config';

export interface PriorityService {
  stopTimeInSeconds: number;
  global: PriorityConfig;
  group: PriorityConfig;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {TemplateResource} from './template-resource';
import {ServerInstallablePlugin} from './server-installable-plugin';

export interface Template {
  name: string;
  backend: TemplateResource;
  url: string;
  processPreParameters: Array<string>;
  intallablePlugins: Array<ServerInstallablePlugin>;
}

import {UUID} from 'angular2-uuid';

export interface Analytics {
  enabled: boolean,
  ID: String
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export enum ServerGroupType {
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  CUSTOM = 'CUSTOM',
  GLOWSTONE = 'GLOWSTONE',
  CAULDRON = 'CAULDORN',
  BUKKIT = 'BUKKIT'
}

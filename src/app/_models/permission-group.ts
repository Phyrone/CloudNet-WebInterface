
/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {Permission} from './permission';
import {KeyValueList} from './key-value-list';
import {KeyValueObject} from './key-value-object';

export class PermissionGroup {
  name: string;
  prefix: string;
  suffix: string;
  display: string;
  tagId: number;
  joinPower: number;
  defaultGroup: boolean;
  permissions: Permission[];
  serverGroupPermissions: KeyValueList[];
  options: KeyValueObject[];
  implementGroups: string[];


}

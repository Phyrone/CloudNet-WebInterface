/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {Wrapper} from './wrapper';
import {NetworkInfo} from './network-info';
import {SimpleProxyGroup} from './simple-proxy-group';
import {ProxyProcessMeta} from './proxy-process-meta';

export interface ProxyServer {
  serviceId: ServiceId;
  wrapper: Wrapper;
  networkInfo: NetworkInfo;
  proxyInfo: SimpleProxyGroup;
  processMeta: ProxyProcessMeta;
}

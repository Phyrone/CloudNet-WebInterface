/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export enum ProxyVersion {
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  TRAVERTINE = 'TRAVERTINE',
  BUNGEECORD = 'BUNGEECORD',
  WATERFALL = 'WATERFALL',
  HEXACORD = 'HEXCORD'
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export interface ServiceId {
  group: string;
  id: number;
  uniqueId: string;
  wrapperId: string;
  serverId: string;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export interface User {
  uuid: string;
  name: string;
  token: string;
  password: string;
  permissions: Array<string>;
}

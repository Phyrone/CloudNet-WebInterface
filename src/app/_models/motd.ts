/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

/**
 * Gibt an welche Title Schrift es haben mit
 * @firstLine und
 * @secondLine
 */
export interface Motd {
  firstLine: string;
  secondLine: string;
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

export enum PluginResourceType {
  // noinspection JSUnusedGlobalSymbols
  // noinspection JSUnusedGlobalSymbols
  MASTER = 'MASTER',
  LOCAL = 'LOCAL',
  URL = 'URL',
}

/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {ServerProcessMeta} from './server-process-meta';
import {ServerGroupType} from './server-group-type.enum';
import {ServerInfo} from './server-info';

export interface MinecraftServer {
  serviceId: ServiceId;
  processMeta: ServerProcessMeta;
  groupMode: ServerGroupType;
  serverInfo: ServerInfo;
}

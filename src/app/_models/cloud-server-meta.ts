/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {ServiceId} from './service-id';
import {ServerInstallablePlugin} from './server-installable-plugin';
import {ServerConfig} from './server-config';
import {ServerGroupType} from './server-group-type.enum';
import {Template} from './template';

export interface CloudServerMeta {
  serviceId: ServiceId;
  memory: number;
  priorityStop: boolean;
  processParameters: string[];
  plugins: ServerInstallablePlugin[];
  serverConfig:ServerConfig;
  port: number;
  templateName: string;
  serverGroupType: ServerGroupType;
  template: Template;
}

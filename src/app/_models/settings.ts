/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {SettingsInterval} from './settingsInterval';

export interface Settings {
  timeout: number;
  branding: string;
  interval: SettingsInterval;
}

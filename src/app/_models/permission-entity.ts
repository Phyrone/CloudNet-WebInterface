/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {KeyValueLong} from './key-value-long';
import {KeyValueBoolean} from './key-value-boolean';

export interface PermissionEntity {
  uniqueId: string;
  permissions: KeyValueBoolean[];
  prefix: string;
  suffix: string;
  groups: KeyValueLong[];
}

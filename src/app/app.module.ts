/*
 * Copyright (c) 2019.
 * CloudNet-WebInterface von Phillipp Glanz ist lizenziert unter einer Creative Commons
 *  Namensnennung -
 *  Nicht kommerziell -
 *  Keine Bearbeitungen 4.0 International Lizenz.
 */

import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';


import {AppComponent} from './app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {AppConfigService} from './_services/app-config.service';
import {MissingTranslationHandler, TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';

import {
  MatAutocompleteModule,
  MatBadgeModule,
  MatBottomSheetModule,
  MatButtonModule,
  MatButtonToggleModule,
  MatCardModule,
  MatCheckboxModule,
  MatChipsModule,
  MatDatepickerModule,
  MatDialogModule,
  MatDividerModule,
  MatExpansionModule,
  MatGridListModule,
  MatIconModule,
  MatInputModule,
  MatListModule,
  MatMenuModule,
  MatNativeDateModule,
  MatPaginatorModule,
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatRadioModule,
  MatRippleModule,
  MatSelectModule,
  MatSidenavModule,
  MatSliderModule,
  MatSlideToggleModule,
  MatSnackBarModule,
  MatSortModule,
  MatStepperModule,
  MatTableModule,
  MatTabsModule,
  MatToolbarModule,
  MatTooltipModule,
  MatTreeModule,
} from '@angular/material';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {AppRoutingModule} from './_routing/app-routing.module';
import {MyDashboardComponent} from './pages/dashboard/my-dashboard.component';
import {ChartsModule} from 'ng2-charts';
import {FlexLayoutModule} from '@angular/flex-layout';
import {LoginComponent} from './pages/login/login.component';
import {AuthenticationService} from './_services/authentication.service';
import {CloudConnectorService} from './_services/cloud-connector.service';
import {AddDialog, DeleteDialog, EditDialog, ManageDialog, ProxygroupsComponent} from './pages/proxygroups/proxygroups.component';
import {
  AddUserDialog,
  DeleteUserDialog,
  EditPermissionDialog,
  UsermangmentComponent,
  UserPasswordDialog
} from './pages/usermangment/usermangment.component';
import {
  ServerAddDialog,
  ServerDeleteDialog,
  ServerEditDialog,
  ServergroupsComponent,
  ServerManageDialog,
  ServerTemplateDialog
} from './pages/servergroups/servergroups.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {MasterStopDialogComponent} from './popup/masterstop/master-stop-dialog.component';
import {SnotifyModule, SnotifyService, ToastDefaults} from 'ng-snotify';
import {APP_BASE_HREF, CommonModule} from '@angular/common';
import {ProxyListComponent} from './pages/proxylist/proxy-list.component';
import {ProxyScreenComponent} from './popup/proxy-screen/proxy-screen.component';
import {ServerListComponent} from './pages/serverlist/server-list.component';
import {ServerScreenComponent} from './popup/server-screen/server-screen.component';
import {CpermsComponent} from './pages/cperms/cperms.component';
import {PermissionGroupComponent} from './pages/cperms/permission-group/permission-group.component';
import {PermissionNavigationComponent} from './pages/cperms/permission-navigation/permission-navigation.component';
import { PermissionUserComponent } from './pages/cperms/permission-user/permission-user.component';
import {MasterInterfaceComponent} from './popup/master-interface/master-interface.component';
import { SearchUserDialogComponent } from './pages/cperms/search-user-dialog/search-user-dialog.component';
import { AddGroupComponent } from './pages/cperms/add-group/add-group.component';
import { PlayerConsoleComponent } from './popup/player-console/player-console.component';
import {CookieService} from 'ngx-cookie-service';
import {NgxCaptchaModule} from 'ngx-captcha';
import { ClientSettingsComponent } from './pages/client-settings/client-settings.component';
import { DisclaimerComponent } from './pages/disclaimer/disclaimer.component';
import { ChangelogComponent } from './pages/changelog/changelog.component';
import { AboutComponent } from './pages/about/about.component';
import {NgxElectronModule} from 'ngx-electron';
import {AuthGuard} from './_guards/auth-guard.guard';
import { SafeHtmlPipe } from './_services/safe-html.pipe';
import { MillisInTimePipe } from './_services/millis-in-time.pipe';
import { WrapperListComponent } from './pages/wrapper-list/wrapper-list.component';
import { MemoryPipe } from './_services/memory.pipe';
import { WrapperDetailViewComponent } from './popup/wrapper-detail-view/wrapper-detail-view.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { SignComponent } from './pages/sign/sign.component';
import { SignEditComponent } from './popup/sign-edit/sign-edit.component';
import { GroupSignCreateComponent } from './popup/group-sign-create/group-sign-create.component';
import {UUID} from 'angular2-uuid';
import { EditSearchAnimationComponent } from './popup/edit-search-animation/edit-search-animation.component';
import {MatomoModule} from 'ngx-matomo';


@NgModule({
  exports: [
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    RouterModule,
    OverlayModule,
    CommonModule,
    TranslateModule
  ],
  declarations: [
    AppComponent,
    MyDashboardComponent,
    LoginComponent,
    ProxygroupsComponent,
    EditDialog,
    ManageDialog,
    DeleteDialog,
    AddDialog,
    MasterInterfaceComponent,
    UsermangmentComponent,
    EditPermissionDialog,
    DeleteUserDialog,
    AddUserDialog,
    UserPasswordDialog,
    ServergroupsComponent,
    ServerEditDialog,
    ServerAddDialog,
    ServerManageDialog,
    ServerDeleteDialog,
    ServerTemplateDialog,
    MasterStopDialogComponent,
    ProxyListComponent,
    ProxyScreenComponent,
    ServerListComponent,
    ServerScreenComponent,
    CpermsComponent,
    PermissionGroupComponent,
    PermissionNavigationComponent,
    PermissionUserComponent,
    SearchUserDialogComponent,
    AddGroupComponent,
    PlayerConsoleComponent,
    ClientSettingsComponent,
    DisclaimerComponent,
    ChangelogComponent,
    AboutComponent,
    SafeHtmlPipe,
    MillisInTimePipe,
    WrapperListComponent,
    MemoryPipe,
    WrapperDetailViewComponent,
    SignComponent,
    SignEditComponent,
    GroupSignCreateComponent,
    EditSearchAnimationComponent
  ],
  entryComponents: [
    ProxygroupsComponent,
    EditDialog,
    ManageDialog,
    DeleteDialog,
    AddDialog,
    EditPermissionDialog,
    DeleteUserDialog,
    AddUserDialog,
    UserPasswordDialog,
    ServerEditDialog,
    ServerAddDialog,
    ServerManageDialog,
    ServerDeleteDialog,
    ServerTemplateDialog,
    MasterStopDialogComponent,
    ProxyScreenComponent,
    ServerScreenComponent,
    MasterInterfaceComponent,
    SearchUserDialogComponent,
    PlayerConsoleComponent,
    ClientSettingsComponent,
    DisclaimerComponent,
    ChangelogComponent,
    AboutComponent,
    WrapperDetailViewComponent,
    SignEditComponent,
    GroupSignCreateComponent,
    EditSearchAnimationComponent
  ],
  imports: [
    BrowserModule,
    MatomoModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    AppRoutingModule,
    OverlayModule,
    ChartsModule,
    SnotifyModule,
    HttpClientModule,
    NgxElectronModule,
    NgxCaptchaModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      useDefaultLang: true
    }),
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })

  ],
  providers: [
    AppConfigService,
    CloudConnectorService,
    AuthGuard,
    CookieService,
    AuthenticationService,
    {
      provide: APP_INITIALIZER,
      useFactory: initializeApp,
      deps: [AppConfigService],
      multi: true,


    }, {
      provide: 'SnotifyToastConfig',
      useValue: ToastDefaults
    },
    SnotifyService,
    AppRoutingModule,
    { provide: APP_BASE_HREF,
      useValue: window['_app_base'] || '/'
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function initializeApp(appConfig: AppConfigService) {
  return () => appConfig.load();
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, 'https://assets.madfix.me/projects/MaterialDesignWebInterface/languages/', '.json?id='+UUID.UUID());
}

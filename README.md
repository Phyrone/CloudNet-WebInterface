# CloudNet-WebInterface

A simple cloudnet web interface, based on material design and angular cli

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">CloudNet-WebInterface</span> von <a xmlns:cc="http://creativecommons.org/ns#" href="https://gitlab.com/themeinerlp/CloudNet-WebInterface" property="cc:attributionName" rel="cc:attributionURL">Phillipp Glanz</a> ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/">Creative Commons Namensnennung - Nicht kommerziell - Keine Bearbeitungen 4.0 International Lizenz</a>.
